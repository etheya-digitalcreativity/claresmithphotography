// console.log(wyvern_config);

//https://developers.google.com/youtube/iframe_api_reference
//http://jsfiddle.net/KtbYR/5/
//http://stackoverflow.com/questions/8948403/youtube-api-target-multiple-existing-iframes
//http://stackoverflow.com/questions/5660116/unsafe-javascript-attempt-to-access-frame-in-google-chrome
//https://vimeo.com/forums/topic:28492

if (typeof window.WyvernVideo == "undefined") {
    window.WyvernVideo = {};
}

WyvernVideo.dialog = {};
WyvernVideo.fields = {};

// Some global setting names may not match up to the service's names.
WyvernVideo.filter_param_map = {
    user: {
        youtube: 'author',
        vimeo: 'user_id'
    }
};

WyvernVideo.feed_params = {
    allow_empty_search: {
        youtube: true,
        vimeo: false
    }
}

WyvernVideo.Handler = {

    init: function(editor, element)
    {
        // console.log('WyvernVideo.Handler.init');

        // If we have an element, then we need to insert into some other field such as the native RTE
        if (editor)
        {
            editor.$editor.delegate('img', 'dblclick', $.proxy(function(e){
                var img = $(e.target);
                img.css({cursor: 'pointer'});
                WyvernVideo.Field.init(WyvernVideo.dialog, {editor: editor, fake_image: img}, true);
            }, this));
        }
        // Bind to any existing custom Wyvern Video field on the Publish page.
        else
        {
            this.getFieldData();

            $('.publishPageContents, .pageContents').delegate('.wyvern-video-field-btn-add', 'click', $.proxy(function(e){
                var field_name = $(e.target).attr('rel').replace(/\[/g, '').replace(/\]/g, '');
                var field_config_name = $(e.target).data('field-config') || false;
                var target = '.wyvern-video-preview-'+ field_name.replace(/\[/g, '').replace(/\]/g, '');

                WyvernVideo.Field.init(WyvernVideo.dialog, {
                    field_name: field_name, 
                    field_config_name: field_config_name,
                    return_target: target
                }, true);

                e.preventDefault();
            }, this));

            $('.publishPageContents, .pageContents').delegate('.wyvern-video-field-btn-remove', 'click', $.proxy(function(e){
                var field_name = $(e.target).attr('rel');
                var target = '.wyvern-video-preview-'+ field_name;

                $(target).addClass('wyvern-video-preview-empty').html('');

                // Empty all the fields.
                $('.wyvern-video-field-data-attr-'+ field_name).val('');
                $('.wyvern-video-field-btn-add-'+ field_name).html('<span></span>Add Video');

                $(e.target).hide();

                e.preventDefault();
            }, this));
        }
    },

    getFieldData: function()
    {
        // Get any existing data from video fields and add them to our collection.
        $('.wyvern-video-field-data-attr').each(function()
        {
            var field = $(this);
            var attr = field.data('attr-name');
            var value = field.val();
            var field_name = field.data('field-name');

            if (typeof WyvernVideo.fields[field_name] == 'undefined')
            {
                WyvernVideo.fields[field_name] = {};
            }

            WyvernVideo.fields[field_name][attr] = value;
        });
    }

};

$(function(){

    WyvernVideo.dialog = $('#wyvern-video-dialog-jquery').dialog({
        width: 870,
        height: 480,
        resizable: false,
        modal: true,
        autoOpen: false,
        title: 'Video',
        position: ['center', 100]
    });

    // Bind all custom fields.
    WyvernVideo.Handler.init();
});

WyvernVideo.Field = {

    init: function(dialog, args, open_dialog) {

        // console.log('WyvernVideo.Field.init');

        this.dialog = dialog;

        this.field_name = false;
        this.field_config_name = false;
        this.field_settings = {};
        this.return_target = false;
        this.editor = false;
        this.fake_image = false;
        this.range = false;
        this.is_single = false;
        this.ignore_params = [];

        // Open dialog after init?
        this.open_dialog = open_dialog || false;

        // A Wyvern CKEditor field
        if ($.isEmptyObject(args))
        {
            // Dig through the CKEditor obj to find the ID of the field we replaced
            this.field_name = this.dialog._.editor.element.$.id;
            this.field_config_name = this.field_name;
        }
        // A Wyvern Video Field
        else if (typeof args.field_name != 'undefined')
        {
            this.field_name = args.field_name;
            this.field_config_name = args.field_config_name;
            this.return_target = args.return_target;
        }
        // An RTE field
        else if (typeof args.editor != 'undefined')
        {
            // console.log(args.editor.$editor);
            this.field_name = args.editor.$editor.attr('id').replace('-editor', '');
            this.field_config_name = args.field_config_name;

            // If we have an RTE existing img dblclick
            if (typeof args.fake_image != 'undefined')
            {
                this.editor = args.editor;
                this.fake_image = args.fake_image;
                this.return_target = args.editor;
            }
            // If we have an RTE button click
            else
            {
                this.editor = args.editor;
                this.editor_state = args.editor_state;
                this.return_target = this.editor.Selection.$editor;

                // Create a basic Range for inserting the fake image.
                var sel = window.getSelection();
                this.range = document.createRange();
                this.range.setStart(sel.anchorNode, sel.anchorOffset);
                this.range.setEnd(sel.focusNode, sel.focusOffset);
            }
        }

        // A few vars used when searching
        this.feed = 'youtube'; // displayed by default
        this.query ='';
        this.offset = 1;
        this.perPage = 12;
        this.currentPage = 1;
        this.searchByUser = false;
        this.searchUsername = '';
        this.searchOnLoad = false;
        this.allowEmptySearch = true; // Default of YouTube allows empty string searches
        this.feedContainer = false;
        this.feedContainerId = false;
        this.urlYouTube = 'http://www.youtube.com/embed/{{videoId}}?rel=0&wmode=opaque&hd=1';
        this.urlVimeo = 'http://player.vimeo.com/video/{{videoId}}?title=0&byline=0&portrait=0';
        this.embedTemplate = '<iframe src="{{url}}" border="0" height="{{height}}" width="{{width}}"></iframe>';

        // Video file attributes
        this.attr = {
            id: '',
            url: '',
            // Set global default dimensions
            width: '360',
            height: '240',
            type: 'youtube',
            title: '',
            description: '',
            thumbnail_small: '',
            thumbnail_large: '',
            author: '',
            length: '',
            embed: '',
            last_query: ''
        };

        // Override some attr if defined as a field setting
        if (this.field_config_name && wyvern_config.wyvern_video[this.field_config_name])
        {
            this.field_settings = wyvern_config.wyvern_video[this.field_config_name]['settings'];

            this.attr.width = this.field_settings.video_width || wyvern_config.wyvern_video.settings_global.global_width;
            this.attr.height = this.field_settings.video_height || wyvern_config.wyvern_video.settings_global.global_height;
        }

        // Attributes to show in display_field()
        this.attrDisplay = [
            'url',
            'title',
            'description'
        ];

        // Search all fields for hidden inputs containing data before trying to set our WyvernVideo.fields array below.
        WyvernVideo.Handler.getFieldData();

        // If this is a Wyvern Video field with data, update our attributes.
        if (this.field_name && typeof WyvernVideo.fields[this.field_name] != 'undefined')
        {
            for (attr in this.attr)
            {
                if(WyvernVideo.fields[this.field_name][attr] != '')
                {
                    this.attr[attr] = WyvernVideo.fields[this.field_name][attr];
                }
            }
        }

        // Annnnd Go!
        this.setDialogContainer();
        this.buildDialog();

        if (this.open_dialog)
        {
            WyvernVideo.dialog.dialog("open");
            this.bindButtons();
        }
    },

    buildDialog: function() {

        // Its is a CKeditor fake image
        if (this.dialog.fakeImage)
        {
            var iframe = unescape( $(this.dialog.fakeImage.$).attr('data-cke-realelement') );

            this.attr.type = $(iframe).attr('data-video-type');
            this.attr.id = $(iframe).attr('data-video-id');

            var fake_image_width = parseInt( this.dialog.fakeImage.$.style.width, 10 );

            if ( !isNaN( fake_image_width ) )
            {
                this.attr.width = fake_image_width;
            }

            var fake_image_height = parseInt( this.dialog.fakeImage.$.style.height, 10 );

            if ( !isNaN( fake_image_height ) )
            {
                this.attr.height = fake_image_height;
            }
        }

        // Its an RTE fake image
        if (this.fake_image) 
        {
            var iframe = unescape( $(this.fake_image).attr('data-cke-realelement') );

            this.attr.type = $(this.fake_image).attr('data-video-type');
            this.attr.id = $(this.fake_image).attr('data-video-id');

            var fake_image_width = parseInt( $(this.fake_image).width(), 10 );

            if ( !isNaN( fake_image_width ) )
            {
                this.attr.width = fake_image_width;
            }

            var fake_image_height = parseInt( $(this.fake_image).height(), 10 );

            if ( !isNaN( fake_image_height ) )
            {
                this.attr.height = fake_image_height;
            }
        }

        var html = '<div class="wyvern-video-dialog-preview-container"> \
            <div class="wyvern-video-dialog-preview"></div> \
            <div class="wyvern-video-dialog-preview-options"></div> \
        </div> \
        <div class="wyvern-video-dialog-list"> \
            <div class="wyvern-video-dialog-search"> \
                <form class="wyvern-video-dialog-search-form"><input type="search" placeholder="Search or URL" class="wyvern-video-dialog-search-input" value="'+ (this.lastQuery || this.attr.last_query || '') +'" /> \
                <input type="submit" class="wyvern-video-dialog-search-submit" value="Search" /></form> \
                <div class="wyvern-video-dialog-tabs-container"> \
                    <ul class="wyvern-video-dialog-tabs"> \
                        <li rel="youtube" class="ui-widget-header active">YouTube</li> \
                        <li rel="vimeo" class="ui-widget-header">Vimeo</li> \
                    </ul> \
                </div> \
                <div class="extra-params"></div> \
            </div> \
            <div class="wyvern-video-dialog-list-container"> \
                <div class="wyvern-video-dialog-list-services-container"> \
                    <div class="wyvern-video-dialog-list-service wyvern-video-dialog-list-youtube"> \
                        <table class="mainTable" border="0" cellspacing="0" cellpadding="0"></table> \
                    </div> \
                    <div class="wyvern-video-dialog-list-service wyvern-video-dialog-list-vimeo" style="display: none;"> \
                        <table class="mainTable" border="0" cellspacing="0" cellpadding="0"></table> \
                    </div> \
                </div> \
                <div class="wyvern-video-dialog-pagination"> \
                    <span class="dataTables_paginate paging_full_numbers" id="filter_pagination"> \
                        <span class="paginate_button previous wyvern-video-dialog-pagination-button" rel="prev"> \
                            <img src="'+ wyvern_config.cp_theme_url +'images/pagination_prev_button.gif" width="13" height="13" alt="&lt; &lt;"> \
                        </span> \
                        <span class="wyvern-video-dialog-pagination-current pagination-current-youtube">1</span> \
                        <!-- <span class="wyvern-video-dialog-pagination-current pagination-current-vimeo">1</span> --> \
                        <span class="paginate_button next wyvern-video-dialog-pagination-button" rel="next"> \
                            <img src="'+ wyvern_config.cp_theme_url +'images/pagination_next_button.gif" width="13" height="13" alt="&lt; &lt;"> \
                        </span> \
                    </span> \
                </div> \
            </div> \
        </div> \
        <div style="clear:both;"></div>';

        $('#'+ this.feedContainerId).html(html);

        // If we already have an ID, show the preview.
        if (this.attr.id)
        {
            this.updatePreview();
        }

        this.getSearchOnLoad(this.attr.type);
        this.getSearchByUserSettings(this.attr.type);

        // If we had a prev query, load the dialog with that query again.
        if (this.attr.last_query || this.searchOnLoad)
        {
            // Don't know why, but this is undefined? @todo
            var query = this.attr.last_query || '';

            $('.wyvern-video-dialog-tabs-container').show();
            this.getFeed(query, this.attr.type);
        }

        if (this.field_settings['allow_resize'] != 'yes')
        {
            var html = '<div class="wyvern-video-dialog-field">ID: <input tabindex="1" type="text" class="wyvern-video-dialog-data-attr-id" value="'+ this.attr.id +'" /></div> \
                <div class="wyvern-video-dialog-field">Width: <p>'+ (this.attr.width || "") +'</p></div> \
                <div class="wyvern-video-dialog-field">Height: <p>'+ (this.attr.height || "") +'</p></div> \
                <input type="hidden" class="wyvern-video-dialog-data-attr-type" value="'+ (this.attr.type || "") +'" />';
        }
        else
        {
            var html = '<div class="wyvern-video-dialog-field">ID: <input tabindex="1" type="text" class="wyvern-video-dialog-data-attr-id" value="'+ this.attr.id +'" /></div> \
                <div class="wyvern-video-dialog-field">Width: <input tabindex="2" type="text" class="wyvern-video-dialog-data-attr-width" value="'+ (this.attr.width || "") +'" /></div> \
                <div class="wyvern-video-dialog-field">Height: <input tabindex="3" type="text" class="wyvern-video-dialog-data-attr-height" value="'+ (this.attr.height || "") +'" /></div> \
                <input type="hidden" class="wyvern-video-dialog-data-attr-type" value="'+ (this.attr.type || "") +'" />';
        }

        $('#'+ this.feedContainerId +' .wyvern-video-dialog-preview-options').html(html);

        $('.wyvern-video-dialog-search-form').submit($.proxy(function(e)
        {
            this.showFilters(this.feed);

            // get/set this.query
            this.getQueryValue();

            // Temporary, re-write the pagination to be specific to each video type.
            this.offset = 1;

            // Do it!
            this.getFeed(this.query, this.feed);

            // Save the query
            this.attr.last_query = this.query;

            e.preventDefault();
        }, this));

        $('.wyvern-video-dialog-tabs li').click($.proxy(function(e)
        {
            var link = $(e.target);
            var rel = link.attr('rel');
            link.addClass('active').siblings().removeClass('active');
            $('.wyvern-video-dialog-list-service').hide();
            $('.wyvern-video-dialog-list-'+ rel).show();
            $('.wyvern-video-dialog-pagination-current').text('1');

            this.getQueryValue();

            // Temporary, re-write the pagination to be specific to each video type.
            this.offset = 1;

            // $('.wyvern-video-dialog-pagination-current').hide();
            // $('.pagination-current-'+ this.feed).show();

            this.feed = rel;

            this.getFeed(this.query, this.feed);
        }, this));

        $('.wyvern-video-dialog-pagination-button').click($.proxy(function(e)
        {
            var dir = $(e.delegateTarget).attr('rel');

            if(dir == 'prev')
            {
                this.offset = this.offset <= this.perPage ? 1 : this.offset - this.perPage;
            }
            else if(dir == 'next')
            {
                this.offset = this.offset + this.perPage;
            }

            this.currentPage = Math.ceil(this.offset / this.perPage);

            $('.wyvern-video-dialog-pagination-current').text(this.currentPage);
            
            this.getFeed(this.query, this.feed);
        }, this));

        $('#'+ this.feedContainerId).delegate('.extra-params .toggle', 'click', $.proxy(function(e){
            var self = $(e.target);
            var attr = self.attr('rel');

            if (self.hasClass('remove'))
            {
                self.closest('p').addClass('inactive');
                self.removeClass('remove').addClass('add');

                if ($.inArray(attr, this.ignore_params) == -1)
                {
                    this.ignore_params.push(attr);
                }
            }
            else
            {
                self.closest('p').removeClass('inactive');
                self.removeClass('add').addClass('remove');

                for (i = 0; i < this.ignore_params.length; i++)
                {
                    this.ignore_params.splice(i, 1);
                }
            }

            this.getFeed(this.query, this.feed);

            e.preventDefault();
        }, this));
    },

    getQueryValue: function()
    {
        this.query = $('.wyvern-video-dialog-search-input').val();
    },

    // See if we should automatically show all of a user's videos when the dialog is loaded.
    getSearchByUserSettings: function(type)
    {
        var type = type || this.attr.type;

        if (wyvern_config.wyvern_video['settings_'+ type])
        {
            if (wyvern_config.wyvern_video['settings_'+ type]['user'] != '' &&
                $.inArray('user', this.ignore_params) == -1)
            {
                this.searchByUser = true;
                this.searchUsername = wyvern_config.wyvern_video['settings_'+ type]['user'];
            }
            else
            {
                this.searchByUser = false;
                this.searchUsername = '';
            }
        }
    },

    getSearchOnLoad: function(type)
    {
        var type = type || this.attr.type;

        if (wyvern_config.wyvern_video['settings_'+ type])
        {
            if (wyvern_config.wyvern_video['settings_'+ type]['show_on_load'] == 'y')
            {
                this.searchOnLoad = true;
            }
            else
            {
                this.searchOnLoad = false;
            }
        }
    },

    getAllowEmptySearch: function(type)
    {
        var type = type || this.attr.type;

        this.allowEmptySearch = WyvernVideo.feed_params.allow_empty_search[type];
    },

    showFilters: function(type)
    {
        type = type ? type : this.feed;

        $('.wyvern-video-dialog-tabs-container').show();
        $('.wyvern-video-dialog-tabs li').show();
        $('.wyvern-video-dialog-pagination').show();
        $('.wyvern-video-dialog-tabs li.'+ type).addClass('active').siblings().removeClass('active');
    },

    setDialogContainer: function()
    {
        // Its a CKeditor dialog...
        if (typeof this.dialog.definition != 'undefined')
        {
            var feedContainer = this.dialog.getContentElement( 'info', 'feedContainer' );
            this.feedContainerId = 'wyvern-video-dialog-contents-ckeditor';

            // Removing the "General" tab. Not sure how else to do this...
            $('.cke_dialog_tab_selected').remove();
        }
        // Its a jQuery dialog...
        else
        {
            this.feedContainerId = 'wyvern-video-dialog-contents-jquery';

            // Add the buttons so they match CKeditor's style.
            if ($(this.dialog).next('.cke_dialog_footer_buttons').length == 0)
            {
                $(this.dialog).after('<table class="cke_dialog_ui_hbox cke_dialog_footer_buttons"><tbody><tr class="cke_dialog_ui_hbox"><td class="cke_dialog_ui_hbox_first"><a style="-webkit-user-select: none;" href="#" title="Cancel" hidefocus="true" class="cke_dialog_ui_button cke_dialog_ui_button_cancel"><span class="cke_dialog_ui_button submit">Cancel</span></a></td><td class="cke_dialog_ui_hbox_last"><a style="-webkit-user-select: none; " href="#" title="OK" hidefocus="true" class="cke_dialog_ui_button cke_dialog_ui_button_ok"><span class="cke_dialog_ui_button submit">OK</span></a></td></tr></tbody></table>');
                $('.cke_dialog_ui_button').css( {"text-decoration": "none"} );
            }
        }
    },

    resetAttr: function()
    {
        for (attr in this.attr)
        {
            this.attr[attr] = '';
        }
    },

    getVideoUrl: function()
    {
        switch(this.attr.type)
        {
            case 'youtube': 
                var url = this.urlYouTube.replace('{{videoId}}', this.attr.id);
            break;
            
            case 'vimeo':
                var url = this.urlVimeo.replace('{{videoId}}', this.attr.id);
            break;
        }

        return url;
    },

    getEmbed: function()
    {
        var embed = this.embedTemplate.replace('{{url}}', this.attr.url)
                                    .replace('{{height}}', this.attr.height)
                                    .replace('{{width}}', this.attr.width);

        return escape(embed);
    },

    updatePreview: function() 
    {
        $('#'+ this.feedContainerId +' .wyvern-video-dialog-preview-options').show();

        var url = this.getVideoUrl();
        var html = '<iframe src="'+ url +'" border="0" height="240" width="360"></iframe>';
        
        $('.wyvern-video-dialog-data-attr-id').val(this.attr.id);
        $('.wyvern-video-dialog-data-attr-type').val(this.attr.type);

        this.attr.url = url;
        this.attr.embed = this.getEmbed();

        $('#'+ this.feedContainerId +' .wyvern-video-dialog-preview').html(html);
    },

    bindButtons: function()
    {
        var ok_button = $('.cke_dialog_ui_button_ok');
        var cancel_button = $('.cke_dialog_ui_button_cancel');

        // If we don't do this, every time a dialog is opened we end up binding a new click event, thus
        // content ends up getting inserted over and over again.
        ok_button.unbind('click');
        cancel_button.unbind('click');

        ok_button.click($.proxy(function(e) {
            var video_info = '';

            // If we have no id, stop here as to not erase data from an existing field.
            if ( ! this.attr.id)
            {
                this.dialog.dialog('close');
                e.preventDefault();
                return;
            }

            // See if the values were overwritten from the fields in the dialog and update the attributes
            for (attr in this.attr)
            {
                if ($('.wyvern-video-dialog-data-attr-'+ attr).length > 0)
                {
                    this.attr[attr] = $('.wyvern-video-dialog-data-attr-'+ attr).val();
                }
            }

            // Its a native RTE field on toolbar button click
            if (this.range && this.attr.id)
            {
                var img = '<img src="'+ this.attr.thumbnail_large +'" data-cke-realelement="'+ this.getEmbed() +'" data-video-type="'+ this.attr.type +'" data-video-id="'+ this.attr.id +'" width="'+ this.attr.width +'" height="'+ this.attr.height +'" data-query="'+ this.attr.last_query +'" />';

                // Reselect the text/node
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(this.range);

                this.return_target.insertHTML(img);
            }
            // Its a native RTE field, but clicked on existing image, so we already have the range
            else if (this.fake_image)
            {
                $(this.fake_image).attr('data-cke-realelement', this.getEmbed())
                                  .attr('data-video-type', this.attr.type)
                                  .attr('data-video-id', this.attr.id)
                                  .attr('data-query', this.attr.last_query)
                                  .attr('src', this.attr.thumbnail_large)
                                  .width(this.attr.width)
                                  .height(this.attr.height);

                // Trigger the update so the hidden textarea gets the updates
                this.editor.$editor.trigger( EE.rte.update_event );
            }
            // Its a normal Wyvern Video custom field
            else
            {
                // Update our hidden fields.
                for (attr in this.attr)
                {
                    $('.wyvern-video-field-data-attr-'+ attr +'-'+ this.field_name).val( this.attr[attr] );

                    if ($.inArray(attr, this.attrDisplay) != -1 && this.attr[attr] != '')
                    {
                        video_info = video_info + '<tr><th width="30%">'+ attr.charAt(0).toUpperCase() + attr.slice(1) +'</th><td width="70%">'+ this.attr[attr] +'</td></tr>';
                    }
                }

                // Update the embed code now after possible sizing and id fields have been changed manually.
                $('.wyvern-video-field-data-attr-embed-'+ this.field_name).val( this.getEmbed() );

                var url = this.getVideoUrl(this.attr.id);
                var info_td = '';
                var has_info = '';
                // var config_name = this.field_config || this.field_name;

                if (this.field_settings['show_details'] == 'yes')
                {
                    has_info = ' has_info';
                    info_td = '<td class="info"><table>' + video_info + '</table></td>';
                }

                // @TODO add custom embed code option here if(videoType == 'custom')
                var html = '<table class="wyvern-video-field-attr-display" width="'+ this.attr.width +'"> \
                    <tr> \
                        <td class="iframe'+ has_info +'"><iframe src="'+ url +'" border="0" height="'+ this.attr.height +'" width="'+ this.attr.width +'"></iframe></td> \
                        '+ info_td +' \
                    </tr> \
                </table>';

                // Return the iframe to the field
                $(this.return_target).addClass('wyvern-video-preview').removeClass('wyvern-video-preview-empty').html(html);

                // Show the remove button.
                $('.wyvern-video-field-btn-'+ this.field_name).removeClass('assets-disabled').show();
                $('.wyvern-video-field-btn-'+ this.field_name).find('a').show();

                // Change the add button text.
                $('.wyvern-video-field-btn-add-'+ this.field_name).html('<span></span>Change Video');
            }

            this.dialog.dialog('close');
            e.preventDefault();
        }, this));

        cancel_button.click($.proxy(function(e) {
            this.dialog.dialog('close');
            e.preventDefault();
        }, this));
    },

    showError: function(str)
    {
        $('<div class="wyvern-video-dialog-error">'+ str +'</div>').insertAfter('.wyvern-video-dialog-search');
    },

    clearError: function()
    {
        $('.wyvern-video-dialog-error').remove();
    },

    getFeed: function(query, type) 
    {
        // Clear any possible previous messages.
        this.clearError();

        // Does this feed type allow empty search strings?
        this.getAllowEmptySearch(type);

        // On load, do we want to search by user or autosearch?
        this.getSearchByUserSettings(type);

        // console.log('this');
        // console.log(this);
        // console.log('wyvern_config');
        // console.log(wyvern_config);

        // If no query at all or not showing all of a user's videos, 
        // and doesn't allow an empty search, stop here.
        if (! query && ! this.searchByUser && ! this.allowEmptySearch)
        {
            this.showError('Please enter a search term.');
            return false;
        }

        this.is_single = false;

        if (query != '')
        {
            var url = query.match(/^http:\/\/(?:.*?)\.?(youtube|vimeo)\.com\/(watch\?[^#]*v=(\w+)|(\d+))/);

            // See if we have an explicit link to a video URL
            if (url && (url[1] == 'youtube' || url[1] == 'vimeo'))
            {
                type = url[1];
                query = url[2];
                this.is_single = true;

                // Hide our tabs and show our feed
                $('.wyvern-video-dialog-tabs-container').hide();
                $('.wyvern-video-dialog-list-service').hide();
            }
        }

        $('.wyvern-video-dialog-list-container').show();
        $('.wyvern-video-dialog-list-'+ type).show();

        // Just make sure the feed lists are empty...
        $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-service table').html('');

        // If its a full URL search, remove filters if there are any...
        if (this.is_single)
        {
            $('.wyvern-video-dialog-search .extra-params').html('');
        }

        // YouTube API handles searching for video ID or text search with the same call just fine, but
        // Vimeo requires 2 separate API calls, so split out the Vimeo single video lookup below
        // if a valid vimeo URL was entered.

        this.hidePagination();
        var extra_query = this.buildExtraQuery(type);

        if ( ! type || type == 'youtube')
        {
            $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-youtube table').html('<tr><td><div class="indicator"><img src="'+ wyvern_config.cp_global_images +'indicator.gif" /></div></td></tr>');

            var q = 'http://gdata.youtube.com/feeds/api/videos?q='+ query +'&alt=json-in-script&callback=?&max-results='+ this.perPage +'&start-index='+ this.offset + extra_query;

            $.getJSON(q, $.proxy(function(data) 
            {
                $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-youtube table').html('');

                if (data === null || (total_results = data.feed.openSearch$totalResults.$t) == 0)
                {
                    this.noResults(type);
                }

                if (total_results > this.perPage)
                {
                    this.showPagination();
                }

                $.each(data.feed.entry, $.proxy(function(i, item) 
                {
                    var video = item['id']['$t'];

                    // replacement of link
                    video = video.replace('http://gdata.youtube.com/feeds/api/videos/','http://www.youtube.com/watch?v=');
                    
                    // removing link and getting the video ID
                    var videoId = video.replace('http://www.youtube.com/watch?v=','');

                    this.addVideoRow({
                        type: 'youtube'
                        , thumbnail_small: 'http://img.youtube.com/vi/'+ videoId +'/2.jpg'
                        , thumbnail_large: 'http://img.youtube.com/vi/'+ videoId +'/0.jpg'
                        , desc: this.truncate(item['content']['$t'])
                        , author: item['author'][0]['name']['$t']
                        , length: item['media$group']['yt$duration']['seconds']
                        , title: item['title']['$t']
                        , video_id: videoId
                    });

                }, this));
            }, this));
        }

        if (this.is_single && type == 'vimeo' && !isNaN(query))
        {
            // Make sure the list is cleared
            $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-vimeo table').html('');

            // Get only 1 Vimeo video
            this.getVimeoVideo(query);
        }
        else
        {
            if ( ! type || type == 'vimeo')
            {
                $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-vimeo table').html('<tr><td><div class="indicator"><img src="'+ wyvern_config.cp_global_images +'indicator.gif" /></div></td></tr>');

                // If no query, but we are defaulting to a user's videos on load, and it's Vimeo
                // Set the query value to a non-blank value, otherwise Vimeo search will fail.
                // YouTube accepts an empty query search value.
                if ( ! query && this.searchByUser && this.searchUsername && type == 'vimeo')
                {
                    var q = wyvern_config.load_vimeo_url +'&per_page='+ this.perPage +'&page='+ this.offset +'&method=videos.getAll'+ extra_query;
                }
                else
                {
                    var q = wyvern_config.load_vimeo_url +'&per_page='+ this.perPage +'&page='+ this.offset +'&method=videos.search&query='+ query + extra_query;
                }
                
                $.getJSON(q, $.proxy(function(data) 
                {
                    $('#'+ this.feedContainerId +' .wyvern-video-dialog-list-vimeo table').html('');

                    if (data === null || (total_results = data.videos.total) == 0)
                    {
                        this.noResults(type);
                    }

                    if (total_results > this.perPage)
                    {
                        this.showPagination();
                    }

                    var videos = [];

                    $.each(data.videos.video, $.proxy(function(i, video)
                    {
                        this.getVimeoVideo(video);

                    }, this));

                }, this));
            }
        }
    },

    hidePagination: function()
    {
        $('.wyvern-video-dialog-pagination').hide();
    },

    showPagination: function()
    {
        $('.wyvern-video-dialog-pagination').show();
    },

    // Based on whatever settings we have in the module, map them to potential query params for each API.
    // If I ever end up adding more filters, this may need to be refactored a bit.
    buildExtraQuery: function(type)
    {
        // If settings are not defined, just return empty string.
        if ( ! wyvern_config.wyvern_video['settings_'+ type]) return '';

        var query = '';
        var config = wyvern_config.wyvern_video['settings_'+ type];

        if (this.searchByUser && this.searchUsername && $.inArray('user', this.ignore_params) == -1)
        {
            query = query + WyvernVideo.filter_param_map.user[type] +'='+ this.searchUsername;

            if ( ! this.is_single)
            {
                // Make sure this filter toggler is added only once.
                if ($('.wyvern-video-dialog-search .extra-params .attr-user').length == 0)
                {
                    $('.wyvern-video-dialog-search .extra-params').html('<p class="attr-user"><a href="#" class="toggle remove" rel="user" title="Remove filter">x</a> <span>Searching videos by: <i>'+ this.searchUsername +'</i></span></p>');
                }
                else
                {
                    $('.wyvern-video-dialog-search .extra-params .attr-user i').text(this.searchUsername);
                }
            }
        }
        else if (this.searchByUser && this.searchUsername)
        {
            query = query + WyvernVideo.filter_param_map.user[type] +'='+ this.searchUsername;
        }

        return query ? '&'+ query : '';
    },

    getVimeoVideo: function(video)
    {
        if (typeof video != "object")
        {
            video = { id: video };
        }

        $.getJSON(wyvern_config.load_vimeo_url+'&method=videos.getInfo&video_id='+video.id, $.proxy(function(data) 
        {
            this.addVideoRow({
                type: 'vimeo'
                , thumbnail_small: data.video[0].thumbnails.thumbnail[0]._content
                , thumbnail_large: data.video[0].thumbnails.thumbnail[2]._content
                , desc: this.truncate(data.video[0].description)
                , author: data.video[0].owner.display_name
                , length: data.video[0].duration
                , title: data.video[0].title
                , video_id: video.id
            });
        }, this));
    },

    addVideoRow: function(data)
    {
        var row = $('<tr class="'+ data.css_class +'" rel="'+ data.video_id +'"><td> \
            <img src="'+ data.thumbnail_small +'" width="100" /> \
            <h4>'+ data.title +'</h4> \
            <p>'+ data.desc +'</p> \
        </td></tr>');

        row.appendTo('#'+ this.feedContainerId +' .wyvern-video-dialog-list-'+ data.type +' table').click($.proxy(function(e){
            var id = row.attr('rel');

            this.attr.id = id;
            this.attr.type = data.type;
            this.attr.title = data.title;
            this.attr.description = data.desc;
            this.attr.thumbnail_small = data.thumbnail_small;
            this.attr.thumbnail_large = data.thumbnail_large;
            this.attr.length = data.length;
            this.attr.author = data.author;

            this.updatePreview();
        }, this));

        if (this.is_single)
        {
            row.click();
        }
    },

    noResults: function(type)
    {
        var row = $('<tr class="even" rel=""><td> \
            <p>No results found for <b>'+ this.query +'</b></p> \
        </td></tr>');

        row.appendTo('#'+ this.feedContainerId +' .wyvern-video-dialog-list-'+ type +' table');
    },

    truncate: function(str, length)
    {
        length = length || 140;
        str = jQuery.trim(str).substring(0, length).split(" ").slice(0, -1).join(" ");

        if (str != '' && str.length > length)
        {
            str = str + '...';
        }

        return str;
    },

    // Unused
    getRange: function()
    {
        var ranges      = this.editor.Selection.$editor.getRanges(),
            selection   = window.getSelection(),
            hasRange    = !! selection.rangeCount,
            el          = selection.anchorNode;

        if ( hasRange )
        {
            while ( el.nodeType != "1" )
            {
                el = el.parentNode;
            }
        }

        if ( ! el || ! this.editor.Selection.$editor.has( $(el) ).length )
        {
            hasRange = false;
        }

        if ( hasRange )
        {
            this.img_range = selection.getRangeAt(0).cloneRange();
            el = this.editor.Selection.$editor.getRangeElements( this.img_range, WysiHat.Element.getBlocks().join(',') ).get(0);

            if (el !== null)
            {
                if ( $(el).is('li,dt,dd,td') )
                {
                    this.img_range.setStart( el, 0 );
                    this.img_range.setEnd( el, 0 );
                }
                else
                {
                    this.img_range.setStartBefore( el );
                    this.img_range.setEndBefore( el );
                }
            }
            
            this.img_range.collapse(true);
        }
        else
        {
            this.img_range = document.createRange();
            this.img_range.selectNode( this.editor.Selection.$editor.get(0).firstChild );
        }
    }
};

WyvernVideo.Helper = {

    getVideoThumbnail: function(type, id)
    {
        var thumbnail = '';

        if ( ! id || ! type) return thumbnail;

        switch (type)
        {
            case 'youtube':
                return $.jYoutube(id);
            break;

            case 'vimeo':
                $.getJSON(wyvern_config.load_vimeo_url+'&method=videos.getInfo&video_id='+id, function(data) 
                {
                    thumbnail = data.video[0].thumbnails.thumbnail[2]._content;
                });
            break
        }

        return thumbnail;
    }
};

/*
 * jYoutube 1.0 - YouTube video image getter plugin for jQuery
 *
 * Copyright (c) 2009 jQuery Howto
 *
 * Licensed under the GPL license:
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Plugin home & Author URL:
 *   http://jquery-howto.blogspot.com
 *
 */
(function(d){d.extend({jYoutube:function(a,b){if(a===null)return"";b=b===null?"big":b;var c;c=a.match("[\\?&]v=([^&#]*)");a=c===null?a:c[1];return b=="small"?"http://img.youtube.com/vi/"+a+"/2.jpg":"http://img.youtube.com/vi/"+a+"/0.jpg"}})})(jQuery);
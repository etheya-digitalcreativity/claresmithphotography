<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include_once config file
include_once PATH_THIRD.'shortlist/config'.EXT;

/**
 * Shortlist Update Class
 *
 * @package         shortlist_ee_addon
 * @version         2.3.1
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel 
 */
class Shortlist_upd {

	// --------------------------------------------------------------------
	// PROPERTIES
	// --------------------------------------------------------------------

	/**
	 * Version number
	 *
	 * @access      public
	 * @var         string
	 */
	public $version = SHORTLIST_VERSION;

	private $actions = array(
		'add_to_shortlist',
		'remove_from_shortlist',
		'clone_shortlist',
		'reorder_list',
		'clear_shortlist',
		// New in 2.0
		'create_new_shortlist',
		'make_shortlist_default',
		'shortlist_list_edit',
		'act_shortlist_remove_list',
		'act_shortlist_clear_list'
	);



	// --------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------

	/**
	 * Constructor: sets EE instance
	 *
	 * @access      public
	 * @return      null
	 */
	public function __construct()
	{	
		//--------------------------------------------  
		//	Alias to get_instance()
		//--------------------------------------------
		if ( ! function_exists('ee') )
		{
			function ee()
			{
				return get_instance();
			}
		}

		// Define the package path
		ee()->load->add_package_path(PATH_THIRD.'Shortlist');

		// Load libraries...
		ee()->load->library('Shortlist_model');

		// Load other models
		if(!isset(ee()->shortlist_core_model)) Shortlist_model::load_models();
	}

	// --------------------------------------------------------------------

	/**
	 * Install the module
	 *
	 * @access      public
	 * @return      bool
	 */
	public function install()
	{
		// --------------------------------------
		// Install tables
		// --------------------------------------

		ee()->shortlist_item_model->install();
		ee()->shortlist_list_model->install();
		ee()->shortlist_channel_model->install();


		// --------------------------------------
		// Add row to modules table
		// --------------------------------------

		ee()->db->insert('modules', array(
			'module_name'    => SHORTLIST_CLASS_NAME,
			'module_version' => SHORTLIST_VERSION,
			'has_cp_backend' => 'y'
		));
		
		// --------------------------------------
		// Register our actions
		// --------------------------------------
		
		foreach ($this->actions AS $action)
		{
			ee()->db->insert('actions', array(
				'class'  => SHORTLIST_CLASS_NAME,
				'method' => $action
			));
		}

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Uninstall the module
	 *
	 * @return	bool
	 */
	function uninstall()
	{
		// --------------------------------------
		// get module id
		// --------------------------------------

		$query = ee()->db->select('module_id')
		       ->from('modules')
		       ->where('module_name', SHORTLIST_CLASS_NAME)
		       ->get();


		// --------------------------------------
		// remove references from modules
		// --------------------------------------

		ee()->db->where('module_name', SHORTLIST_CLASS_NAME);
		ee()->db->delete('modules');

		// --------------------------------------
		// Uninstall tables
		// --------------------------------------

		ee()->shortlist_item_model->uninstall();
		ee()->shortlist_list_model->uninstall();
		ee()->shortlist_channel_model->uninstall();


		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Update the module
	 *
	 * @return	bool
	 */
	function update($current = '')
	{
		// --------------------------------------
		// Same Version - nothing to do
		// --------------------------------------

		if ($current == '' OR version_compare($current, SHORTLIST_VERSION) === 0)
		{
			return FALSE;
		}

		if( version_compare( $current, '1.0.1' ) < 1 )
		{
			$this->_update_from_101();
		}

		if( version_compare( $current, '1.1.0' ) < 1 )
		{
			$this->_update_from_110();
		}

		if( version_compare( $current, '2.0.0.a1') < 1 ) 
		{
			$this->_update_from_pre_200();
		}

		if( version_compare( $current, '2.2.1') < 1 ) 
		{
			$this->_update_from_220();
		}



		// Get the current actions list and compare to the actions list up top. 
		$current_actions = ee()->db->where('class', SHORTLIST_CLASS_NAME)->get('actions')->result_array();

		$actions = $this->actions;

		foreach( $current_actions as $act )
		{
			if( in_array( $act['method'], $actions ) )
			{
				foreach( $actions as $key => $action )
				{
					if( $action == $act['method'] )
					{
						unset( $actions[ $key ] );
					}
				}
			}
		}

		// Now just add the ones that aren't there
		foreach ($actions AS $action)
		{
			ee()->db->insert('actions', array(
				'class'  => SHORTLIST_CLASS_NAME,
				'method' => $action
			));
		}
	
		// Returning TRUE updates db version number
		return TRUE;
	}

	// --------------------------------------------------------------------


	private function _update_from_101()
	{
		/* There was a misspelling in the config variables 
			for the 1.0.0 release, that will have caused the 
			channel fields to be named incorrectly, this changes the 
			spelling to be correct now */

		// Note the missing 'l' in shortlist_databock
		$sql[] = "UPDATE exp_channel_fields SET field_name = '".SHORTLIST_DATABLOCK_FIELD_NAME."' WHERE field_name = 'shortlist_databock'";

		$sql[] = "UPDATE exp_channel_fields SET field_name = '".SHORTLIST_UNIQUE_FIELD_NAME."' WHERE field_name = 'shortlist_unqiue_val'";

		// Run the queries
		foreach( $sql as $s )
		{
			ee()->db->query( $s );
		}

		return TRUE;
	}

	
	private function _update_from_110()
	{
		/* There was an unessecary use of the channel_entries_tagdata hook 
			which can cause issues
			This clears it out */

		$sql[] = "DELETE FROM exp_extensions WHERE class = 'Shortlist_ext' AND method = 'channel_entries_tagdata'";


		// Run the queries
		foreach( $sql as $s )
		{
			ee()->db->query( $s );
		}

		return TRUE;
	}


	
	private function _update_from_pre_200()
	{

		ee()->shortlist_list_model->install();

		// Collect together all items that aren't in lists and create lists for them
		$sql = "SELECT i.*
		   			FROM exp_shortlist_item i 
						LEFT JOIN exp_shortlist_list l ON l.list_id = i.list_id 
					WHERE l.list_id IS NULL	
						GROUP BY i.list_id ";
		// No create lists for these orphans
		$s = ee()->db->query( $sql );

		foreach( $s->result_array() as $row ) 
		{
			// Create list
			$sess = array();
			// Assign this to the current user
			if( $row['member_id'] != '' AND $row['member_id'] != '0' )
			{
				$sess['key'] = 'member_id';
				$sess['val'] = $row['member_id'];
			}
			elseif( $row['session_id'] != '' )
			{
				$sess['key'] = 'session_id';
				$sess['val'] = $row['session_id'];
			}

			ee()->shortlist_list_model->create( array(), TRUE, $row['list_id'], $sess );

		}

		return TRUE;
	}


	private function _update_from_220()
	{
		// Add our private_list colum to the lists table
		$sql = "ALTER TABLE  `exp_shortlist_list` ADD  `private_list` INT( 10 ) UNSIGNED NOT NULL";

		ee()->db->query($sql);

		return TRUE;
	}
	

} // End class

/* End of file upd.Shortlist.php */
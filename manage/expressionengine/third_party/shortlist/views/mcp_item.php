<div id="shortlist_container" class="mor">

	

	<div class="tg" style="width : 49%; float :left; margin-right : 1%">
		<h2><?=lang('shortlist_details_about_this_item')?></h2>

		<table class="data">
			<tbody>
				<tr>
					<td style="text-align:right; width: 40%;"><?=lang('shortlist_title')?></td>
					<td><strong><?=$item['title']?></strong></td>
				</tr>
				<tr>
					<td style="text-align:right"><?=lang('type')?></td>
					<td><strong><?=lang('shortlist_'.$item['type'])?></strong></td>
				</tr>

				<?php if( $item['type'] == 'Internal' ) : ?>
				<tr>
					<td></td>
					<td><a href="<?=$item['entry_edit_url']?>"><?=lang('shortlist_view_entry')?></a></td>
				</tr>
				<?php endif;?>

			</tbody>

		</table>

		<?php if( $item['type'] == 'External' AND !empty( $item['meta'] ) ) : ?>
		<h3><?=lang('shortlist_external_item_details')?></h3>
		<table class="data">
			<tbody>
				<tr>
					<td style="text-align:right; width: 40%;"><?=lang('shortlist_unique_marker')?></td>
					<td><strong><?=$item['meta']['unqiue']?></strong></td>
				</tr>
				<?php foreach( $item['meta']['datablock'] as $key => $row ) : ?>
				<tr>
					<td style="text-align:right"><?=$key?></td>
					<td><strong><?=$row?></strong></td>
				</tr>
				<?php endforeach; ?>
				

			</tbody>
		</table>
		<?php endif; ?>
	</div>


	<div class="tg" style="width : 49%; float :left">
		<h2><?=lang('shortlist_item_appears_in')?> <?=$list_count?> <?=lang_switch('shortlist_list',$list_count)?></h2>

		<table class="data">
			<thead>
				<tr style="background-color :transparent">
					<th><?=lang('shortlist_user_or_guest')?></th>
					<th><?=lang('item_count')?></th>
					<th><?=lang('shortlist_last_active')?></th>
				</tr>
			</head>
			<tbody>

				<?php foreach( $lists as $list ) : ?>
				<tr>
					<td><a href="<?=$list['list_detail_url']?>">
						<?php if( $list['user_type'] == 'guest' ) : ?>[ <?=lang('shortlist_guest')?> ]
						<?php else : ?><?=$members[ $list['member_id'] ]['screen_name']?><?php endif;?></a></td>
					<td><?=$list['total_item_count']?></td>
					<td><?=$list['last_activity_since']?></td>
				</tr>
				<?php endforeach; ?>

			</tbody>

		</table>

	</div>

	



	

</div>
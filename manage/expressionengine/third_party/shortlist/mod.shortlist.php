<?php if ( ! defined('EXT')) exit('No direct script access allowed');


// include_once config file
include_once PATH_THIRD.'shortlist/config'.EXT;

/**
 * Shortlist Module Class
 *
 * @package         shortlist_ee_addon
 * @version         2.3.1
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel 
 */

class Shortlist {
    
    public $return_data;
    private $EE;

    private $tagdata;
    private $data;
    private $cache;

    private $tag_markers = array('CURRENT_URI', 'CURRENT_URL');
    private $amp = '&';
    /**
     * Constructor: sets EE instance
     *
     * @access      public
     * @return      null
     */
    public function __construct()
    {
        //--------------------------------------------  
        //  Alias to get_instance()
        //--------------------------------------------
        if ( ! function_exists('ee') )
        {
            function ee()
            {
                return get_instance();
            }
        }

        // Load base model
        if( !isset( ee()->shortlist_model ) ) ee()->load->library('Shortlist_model');

        // Is this user anon?
        // We need a proper session for this to work
        $this->_check_session();

        // Do some quick tag param cleaning for our 
        // helper markers
        $this->_clean_tagparams();

        // Load other models
        Shortlist_model::load_models();

    }

    // --------------------------------------------------------------------


    public function item()
    {
        $this->tagdata = ee()->TMPL->tagdata;

        // We require an entry_id or an is_external='yes'
        $entry_id = ee()->TMPL->fetch_param('entry_id');
        $is_external = ee()->TMPL->fetch_param('is_external');  
        if( $entry_id == '' AND $is_external != 'yes' ) return ee()->TMPL->no_results;

        // Also add the lists, and their respective parts
        $lists = ee()->shortlist_list_model->get_lists();

        $default_list = array();
        $default_list_id = '';
        if( !empty( $lists ) )
        {
            foreach( $lists as $list )
            {
                if( $list['default_list'] ) 
                {
                    $default_list = $list;
                    continue;
                }
            }
        }


        if( !empty( $default_list ) ) $default_list_id = $default_list['list_id'];

        // Hand off to item model 
        $item = ee()->shortlist_item_model->get_item('', $default_list_id );
        $all = ee()->shortlist_core_model->everything();

        if(empty($item)) 
        {
            // This doesn't exist in ANY lists 
            $item['not_in_any_list'] = TRUE;
        }
        else
        {
            $item['not_in_any_list'] = FALSE;
        }

        // Add the action urls as appropriate
        $item['add_url'] = $this->shortlist_add();
        $item['remove_url'] = $this->shortlist_remove();
        $item['clear_url'] = $this->shortlist_clear();

        $entry_id = 0;
        if( isset( $item['entry_id'] ) ) $entry_id = $item['entry_id'];
        if( isset( ee()->TMPL->tagparams['entry_id'] ) ) $entry_id = ee()->TMPL->tagparams['entry_id'];

        foreach( $lists as $key => $list )
        {
            $lists[$key]['list_add_url'] = $this->shortlist_add( 0, FALSE, $list['list_id'] );
            $lists[$key]['list_remove_url'] = $this->shortlist_remove( 0, FALSE, $list['list_id'] );
            $lists[$key]['list_clear_url'] = $this->shortlist_clear( $list['list_id'] );

            $lists[$key]['in_list'] = FALSE;

            $this_list = $all['lists'][$list['list_id']];

            // is this item already in this list?


            if( isset( $this_list['items'] ) AND !empty($this_list['items']) AND $entry_id != 0)
            {
                foreach( $this_list['items'] as $i )
                {
                    if( $i['entry_id'] == $entry_id ) 
                    {                       
                        $lists[$key]['in_this_list'] = TRUE;
                        $lists[$key]['in_list'] = TRUE;
                        $item['not_in_any_list'] = FALSE;
       
                        continue;
                    }
                }
            }

        }


        $item['lists'] = $lists;
        $this->data = $item;


        return ee()->TMPL->parse_variables( $this->tagdata, array( $this->data ) );
    }

    // --------------------------------------------------------------------

    public function lists()
    {   
        $this->tagdata = ee()->TMPL->tagdata;

        // Grab any orderby data first
        $orderby = ee()->TMPL->fetch_param('orderby');
        $sort = ee()->TMPL->fetch_param('sort');
        $current_user_only = ee()->shortlist_core_model->check_yes_no('yes', ee()->TMPL->fetch_param('current_user_only'));

        if( $current_user_only ) ee()->shortlist_core_model->filter_by_current_user();


        $lists = array();
        // Limiting to just a single list?
        if( ee()->TMPL->fetch_param('list_id') != '' OR ee()->TMPL->fetch_param('list_url_title') != '' )
        {
            $list = array();
            if( ee()->TMPL->fetch_param('list_id') != '' )
            {
                $list = ee()->shortlist_list_model->get_list( ee()->TMPL->fetch_param('list_id') );
            }
            elseif( ee()->TMPL->fetch_param('list_url_title') != '' )
            {
                $list = ee()->shortlist_list_model->get_list_by_url( ee()->TMPL->fetch_param('list_url_title') );
            }

            if( !empty( $list ) ) $lists[0] = $list;
        }
        elseif( ee()->TMPL->fetch_param('list_url_title_start') != '' OR ee()->TMPL->fetch_param('list_url_title_end') != '' )
        {
            // We might have both, pass them regardless
            $lists = ee()->shortlist_list_model->get_lists_by_start_end( ee()->TMPL->fetch_param('list_url_title_start'), ee()->TMPL->fetch_param('list_url_title_end'), $orderby, $sort);

        }
        else 
        {
            $lists = ee()->shortlist_list_model->get_lists($orderby, $sort);
        }
        if( empty( $lists ) ) return ee()->TMPL->no_results;


        // Do we need the items?
        $marker = 'items';
        $matches = ee()->shortlist_model->match_pair( $this->tagdata, $marker );
        if( $matches !== FALSE ) $items = ee()->shortlist_item_model->get_all();
        else $items = array();

        foreach( $lists as $key => $list )
        {
            $t = $list;
            if( isset( $t['is_default'] ) AND $t['is_default'] == TRUE ) $t['make_default_url'] = '';
            else $t['make_default_url'] = $this->shortlist_set_default( $list['list_id'] );

            $t['remove_list_url'] = $this->shortlist_remove_list( $list['list_id'] );
            $t['clear_list_url'] = $this->shortlist_clear_list( $list['list_id'] );

            $i = array();
            foreach( $items as $item_key => $item )
            {
                if( $item['list_id'] = $list['list_id'] )
                {
                    $i[] = $item;
                    unset( $items[ $item_key ] );
                }
            }

            $t['items'] = $i;
            $lists[ $key ] = $t;
        }


        // Now do some magic to replace the sub-loop items, 
        // as if the user had used a full shortlist tag
        $return = ee()->TMPL->fetch_param('return');
        if( $return != '' ) $return = ' return="'.$return.'"';

        $start = '{exp:shortlist:view list_id="{list_id}"'.$return.'}';
        $end = '{/exp:shortlist:view}';
        $replace = $start .' '.$matches[1].' '.$end;

        $this->tagdata = str_replace( $matches[0], $replace, $this->tagdata );

        $this->data = $lists;

        return ee()->TMPL->parse_variables( $this->tagdata, $lists );
    }

    public function edit_list_form()
    {
        $list_id = ee()->TMPL->fetch_param('list_id');

        if( $list_id == '' )
        {
            // We only want to view the default list items
            $default_list = ee()->shortlist_list_model->get_default_list();
            if( $default_list !== FALSE ) $list_id = $default_list['list_id'];
        }

        $list = ee()->shortlist_list_model->get_create_list( array('list_id' => $list_id) );

        // Add in clear, make default and remove urls too
        if( $list['default_list'] ) $list['make_default_url'] = '';
        else $list['make_default_url'] = $this->shortlist_set_default( $list['list_id'] );

        $list['remove_list_url'] = $this->shortlist_remove_list( $list['list_id'] );
        $list['clear_list_url'] = $this->shortlist_clear_list( $list['list_id'] );


        $t = $this->_wrap_form( 'shortlist_list_edit', ee()->TMPL->tagdata, $list );

        return $t;
    }

    // --------------------------------------------------------------------

    public function item_in_list()
    {
        $exists = ee()->shortlist_item_model->exists_in_list();

        if($exists) return 'yes';

        return 'no';
    }

    // --------------------------------------------------------------------
    public function shortlist_remove( $item_id = 0, $attr = FALSE, $list_id = '')
    {
        $act_url = $this->_get_action_url( 'remove_from_shortlist' );

        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');

        if( $item_id == 0 ) $params = ee()->TMPL->tagparams;
        elseif( $attr !== FALSE ) $params[ $attr ] = $item_id;
        else $params['item_id'] = $item_id;

        if( !isset( $params['list_id'] ) AND $list_id != '' ) $params['list_id'] = $list_id;

        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );

        return $act_url;
    }



    private function _shortlist_toggle( $type = 'add', $keys = array() )
    {
        if( empty( $keys ) ) return '';

//      return print_R(ee()->TMPL->tagparams,1);
    
    //  $real_params = ee()->TMPL->tagparams;

        // Override the params for a moment
        ee()->TMPL->tagparams = $keys;

        if( $type == 'add' ) $ret = $this->shortlist_add();
        else $ret = $this->shortlist_remove();
        
        // Reset the params 
    //  ee()->TMPL->tagparams = $real_params;

        return $ret;    
    }


    // --------------------------------------------------------------------

    public function shortlist_remove_list( $list_id = '' )
    {
        $act_url = $this->_get_action_url( 'act_shortlist_remove_list' );
        $params = array();

        
        if( $list_id == '' ) $params = ee()->TMPL->tagparams;
        else $params['list_id'] = $list_id;

        
        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }



    // --------------------------------------------------------------------

    public function shortlist_clear_list( $list_id = '' )
    {
        $act_url = $this->_get_action_url( 'act_shortlist_clear_list' );
        $params = array();

        
        if( $list_id == '' ) $params = ee()->TMPL->tagparams;
        else $params['list_id'] = $list_id;

        
        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }



    // --------------------------------------------------------------------

    public function shortlist_set_default( $list_id = '' )
    {
        $act_url = $this->_get_action_url( 'make_shortlist_default' );
        $params = array();

        
        if( $list_id == '' ) $params = ee()->TMPL->tagparams;
        else $params['list_id'] = $list_id;

        
        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }

    // --------------------------------------------------------------------

    public function shortlist_add( $item_id = 0, $cloned_from = FALSE, $list_id = '' )
    {
        $act_url = $this->_get_action_url( 'add_to_shortlist' );

        if( $item_id == 0 ) $params = ee()->TMPL->tagparams;
        else $params['item_id'] = $item_id;

        $params['cloned_from'] = $cloned_from;

        if( !isset( $params['list_id'] ) AND $list_id != '' ) $params['list_id'] = $list_id;
        
        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        

        return $act_url;
    }

    // --------------------------------------------------------------------

    public function shortlist_add_list()
    {
        $act_url = $this->_get_action_url('create_new_shortlist');

        $params = ee()->TMPL->tagparams;        
        $params['marker'] = time();

        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }

    // --------------------------------------------------------------------

    public function add()
    {
        return $this->shortlist_add();
    }

    // --------------------------------------------------------------------

    public function remove()
    {
        return $this->shortlist_remove();
    }

    // --------------------------------------------------------------------

    public function clear()
    {
        return $this->shortlist_clear();
    }

    // --------------------------------------------------------------------

    public function clear_all()
    {
        return $this->shortlist_clear('', TRUE);
    }

    // --------------------------------------------------------------------

    public function auto_clear()
    {
        return $this->shortlist_clear_auto();
    }


    // --------------------------------------------------------------------

    public function auto_clear_all()
    {
        return $this->shortlist_clear_auto(TRUE);
    }


    // --------------------------------------------------------------------

    public function add_list()
    {
        return $this->shortlist_add_list();
    }

    // --------------------------------------------------------------------

    public function auto_add_list()
    {
        return $this->shortlist_add_list_auto();
    }


    // --------------------------------------------------------------------

    public function remove_list()
    {
        return $this->shortlist_remove_list();
    }

    // --------------------------------------------------------------------

    public function clear_list()
    {
        return $this->shortlist_clear_list();
    }


    // --------------------------------------------------------------------

    /**
    * Shortlist Add List Auto
    *
    * Automatically adds a list (or group of lists) for a user
    *
    */

    public function shortlist_add_list_auto()
    {
        $keys = ee()->TMPL->tagparams;
        if( !is_array( $keys ) ) return;

        $keys = ee()->security->xss_clean( $keys );

        $make_default = TRUE;
        if( isset( $keys['make_default'] ) AND $keys['make_default'] == 'no' ) $make_default = FALSE;
        // Pass over to the list model
        $data = ee()->shortlist_list_model->create_unless_exists($keys, $make_default, 'list_title');

        if( !is_array( $data ) ) return FALSE;

        if(ee()->TMPL->fetch_param('return') != '') ee()->functions->redirect( ee()->TMPL->fetch_param('return') );

        return '';
    }   


    // --------------------------------------------------------------------

    /**
    * Shortlist Clear Auto
    *
    * Clears a users lists right away and silently
    *
    */
    public function shortlist_clear_auto($all_lists = FALSE)
    {
        $params = ee()->TMPL->tagparams;

        if( $all_lists === TRUE ) {
            $params['all_lists'] = TRUE;
        }
        else {
            // Default to the default list
            $l = ee()->shortlist_list_model->get_default_list();
            $params['list_id'] = $l['list_id'];
        }

        if( !is_array( $params ) OR empty($params) ) exit();

        // Pass over to the list model
        ee()->shortlist_list_model->clear_list( $params );

        if( isset($params['return']) ) {
            $return = urldecode( $params['return']);
            ee()->functions->redirect( $return );
        }

        return;
    }



    // --------------------------------------------------------------------

    public function shortlist_clear( $list_id = '', $all_lists = FALSE )
    {
        $act_url = $this->_get_action_url( 'act_shortlist_clear_list' );

        $params = ee()->TMPL->tagparams;

        if( !isset( $params['list_id'] ) AND $list_id != '' ) $params['list_id'] = $list_id;
        else {
            if( $all_lists === TRUE ) {
                $params['all_lists'] = TRUE;
            }
            else {
                // Default to the default list
                $l = ee()->shortlist_list_model->get_default_list();
                $params['list_id'] = $l['list_id'];
            }
        }

        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }

    // --------------------------------------------------------------------

    public function clone_url()
    {
        return $this->shortlist_clone( ee()->TMPL->fetch_param('list_id') );
    }

    // --------------------------------------------------------------------

    public function shortlist_clone( $list_id = '' )
    {
        if( $list_id == '' ) return '';

        $act_url = $this->_get_action_url( 'clone_shortlist' );

        $params['list_id'] = $list_id;

        // Take any and all params and wrap them up neatly
        ee()->load->helper('string');
        $ret = $this->_get_ret_url($params);
        $params = base64_encode( serialize( $params ) );

        $act_url .= $this->amp.'p=' . $params;
        $act_url .= $this->amp.'ret=' . urlencode( $ret );
        
        return $act_url;
    }

    public function reorder_uri()
    {
        return $this->_get_action_url('reorder_list');
    }

    

    
    // --------------------------------------------------------------------

    public function shortlist_add_remove( $type = 'add' )
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();

        $keys = ee()->security->xss_clean( $keys );
        $list_id = '';

        // Pass over to the list model
        if( $type == 'add' ) $state = ee()->shortlist_item_model->add( $keys );
        else
        {
            if( !isset( $keys['item_id'] ) ) $list_id = '';
            else 
            {
                $item = ee()->shortlist_item_model->get_one( $keys['item_id'] );
                if( !empty( $item ))
                {
                    $list_id = $item['list_id'];
                }

            }

            $state = ee()->shortlist_item_model->remove( $keys );
        } 

        $this->_break_cache();

        if( $this->is_ajax_request())
        {
            // This is an ajax call, don't redirect

            // For the ajax calls we want to return the updated state of the
            // list count, and a new uri to allow the dev to use the action in 
            // for a toggle remove/add

            if( $list_id == '' ) 
            {
                $l = ee()->shortlist_list_model->get_default_list();
                $list_id = $l['list_id'];
            }
            
            $list = ee()->shortlist_item_model->get_list( $list_id );

            $next_type = ( $type == 'add' ? 'remove' : 'add' );
            $verb = ( $type == 'add' ? 'added' : 'removed' );

            $toggle_url = $this->_shortlist_toggle( $next_type, $keys );

            ee()->output->send_ajax_response(array(
                    'type'          => $type,
                    'list_items'    => $list,
                    'total_items'   => count( $list ),
                    'toggle_url'    => $toggle_url,
                    'verb'          => $verb,
                    'list_id'       => $list_id,
                    'keys'          => $keys
                ));

        }

        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }

    public function item_count()
    {
        // Just gets the total count for a list for a user

        // Do we have a list_id as a param?
        $list_id = ee()->TMPL->fetch_param('list_id');


        if( !( ee()->TMPL->fetch_param('all_lists') == 'yes' ) AND $list_id == '')
        {
            // We only want to view the default list items
            $default_list = ee()->shortlist_list_model->get_default_list();
            if( $default_list !== FALSE ) $list_id = $default_list['list_id'];
        }

        if( $list_id != '' )
        {
            // Hand off to item model 
            $items = ee()->shortlist_item_model->get_list( $list_id );
        }
        else
        {
            // Hand off to item model 
            $items = ee()->shortlist_item_model->get_all();
        }

        $count = count( $items );

        return $count;
    }


    // --------------------------------------------------------------------

    public function view( $tagdata = '', $tagparams = array() )
    {
        if( $tagdata != '' ) $this->tagdata = $tagdata;
        else $this->tagdata = ee()->TMPL->tagdata;

        if( !empty( $tagparams ) ) $params = $tagparams;
        else $params = ee()->TMPL->tagparams;
        $this->_log('Getting the view for Shortlist user');

        // If we're viewing a list by list_id, are they the owner?
        $owner = TRUE;

        // Do we have a list_id as a param?
        $list_id = ( isset( $params['list_id'] ) ? $params['list_id'] : '' );

        if( !(isset( $params['all_lists'] ) AND $params['all_lists'] == 'yes') AND $list_id == '')
        {
            // We only want to view the default list items
            $default_list = ee()->shortlist_list_model->get_default_list();
            if( $default_list !== FALSE ) $list_id = $default_list['list_id'];
        }

        if( $list_id != '' )
        {
            $this->_log('Viewing a specific list - list_id = '.$list_id );

            $list = ee()->shortlist_list_model->get_list( $list_id );

            if( !empty($list) )
            {
                // Hand off to item model 
                $items = ee()->shortlist_item_model->get_list( $list['list_id'], FALSE, TRUE );
                // Check if they're the owner of this
                $owner = $this->_is_owner( $items );
            }
        }
        else
        {
            // Hand off to item model 
            $this->_log('Getting this user\'s items');
            $items = ee()->shortlist_item_model->get_all();
        }
        if( empty( $items ) )
        {
            $this->_log('User shortlist is empty, returning no_results');
            return ee()->TMPL->no_results;
        }

        $entry_ids = array();

        foreach( $items as $key => $item )
        {
            $remove_url = ( $owner ? $this->shortlist_remove( $item['item_id'] ) : $this->shortlist_remove( $item['entry_id'], 'entry_id') );
            $add_url = ( $owner ? '' : $this->shortlist_add( $item['item_id'], $list_id ) );

            // Is this item in this users' list?
            $in_list = ( $owner ? TRUE : $this->_in_list( $item['entry_id'] ) );

            $clone_url = ( $owner ? '' : $this->shortlist_clone( $list_id ) );

            $items[ $key ][ 'is_owner' ] = ( $owner ? TRUE : FALSE );
            //$items[ $key ][ 'clone_url' ] = $clone_url;
            $items[ $key ]['remove_url'] = $remove_url;
            $items[ $key ]['add_url'] = $add_url;
            $items[ $key ]['in_list'] = $in_list;
            $items[ $key ]['clone_url'] = $clone_url;
            $entry_ids[] = $item['entry_id'];
        }

        if( empty( $entry_ids ) )
        {
            $this->_log('User shortlist has no entry_ids, returning no_results');
            return ee()->TMPL->no_results;
        }

        // Now we have the items, we can pass this over to the channel model to do the subclassed work.
        $this->_log('Passing entry_ids to channel, entry_ids = '.print_R($entry_ids,1));
        $t = $this->_pass_to_channel( $entry_ids, $items);

        return $t;
    }

    private function _in_list( $entry_id )
    {
        // Do we have a cache?
        if( !isset( $this->cache['user_items'] ) )
        {
            // Pull fresh from db and add to cache
            // Get this user's items
            $user_items = ee()->shortlist_item_model->get_all();

            $this->cache['user_items'] = $user_items;
        }

        // Loop over the user items to check if this entry_id
        // is already in their list
        $in_list = FALSE;

        foreach( $this->cache['user_items'] as $item ) 
        {
            if( $item['entry_id'] == $entry_id ) 
            {
                $in_list = TRUE;
                return $in_list;
            }
        }

        return $in_list;
    }

    // --------------------------------------------------------------------

    private function _is_owner( $items = array() )
    {
        if( empty( $items ) ) return FALSE;

        $row = current( $items );
        
        // If the user is logged in, use that
        if( ee()->session->userdata('member_id') != '0' )
        {
            if( $row['member_id'] == ee()->session->userdata('member_id') ) return TRUE;
        }
        elseif( ee()->session->userdata('session_id') != '0' )
        {
            if( $row['session_id'] == ee()->session->userdata('session_id') ) return TRUE;
        }
        else
        {
            // No member_id or session_id. Use our own
            if( $row['session_id'] == ee()->shortlist_core_model->get_create_session() ) return TRUE;
        }

        return FALSE;
    }
    // --------------------------------------------------------------------


    private function _get_action_url($method_name)
    {
        // Cache the action urls for repeated use
        if( !isset( ee()->session->cache['shortlist']['action_urls'][ $method_name ] ) )
        {
            $action_id  = ee()->db->where(
                array(
                    'class'     => SHORTLIST_CLASS_NAME, 
                    'method'    => $method_name
                )
            )->get('actions')->row('action_id'); 

            ee()->session->cache['shortlist']['action_urls'][ $method_name ] = ee()->functions->fetch_site_index(0, 0) . '?ACT=' . $action_id;
        }

        return ee()->session->cache['shortlist']['action_urls'][ $method_name ];

    }
    //END get_action_url


    // --------------------------------------------------------------------

    private function _pass_to_channel( $entry_ids = array(), $items = array() ) 
    {
        if ( class_exists('Channel') === FALSE )
        {
            require PATH_MOD.'channel/mod.channel'.EXT;
        }

        $this->_log('Passing over to channel');

        $channel = new Channel;

        ee()->TMPL->tagparams['dynamic'] = 'no';
        ee()->TMPL->tagparams['entry_id'] = implode( '|', $entry_ids);
        ee()->TMPL->tagparams['fixed_order'] = implode( '|', $entry_ids);
        ee()->TMPL->tagparams['show_expired'] = 'yes';
        ee()->TMPL->tagparams['show_future_entries'] = 'yes';
        ee()->TMPL->tagparams['status'] = 'not uZxV9NoUqAfx7RsX2Y}d9r.DgnGKBJPb';

        ee()->TMPL->tagparams['is_shortlist'] = 'yes';

        $channel->is_shortlist = TRUE;
        $channel->shortlist_items = $items;

        // Get the unique val and datablock field_ids here for speed
        // so we don't have to requery them later inside the loop
        $meta = ee()->shortlist_channel_model->get_meta('fields');
        $channel->shortlist_fields = $meta;

        $t = $channel->entries();

        return $t;
    }

    // --------------------------------------------------------------------

    /**
     *  AJAX Request
     *
     *  Tests via headers or GET/POST parameter whether the incoming request is AJAX in nature
     *  Useful when we want to change the output of a method.
     *
     *  @access     public
     *  @return     boolean
     */

    public function is_ajax_request()
    {
        if (ee()->input->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest')
        {
            return TRUE;
        }
        

        return FALSE;
    }
    // END is_ajax_request()


    
    private function _check_session()
    {
        if( ee()->session->userdata('member_id') == '0' )
        {
            if( ee()->session->userdata('session_id') == '0' OR ee()->session->userdata('session_id') == FALSE )
            {
            //  ee()->session->create_new_session(0);
            }
        }
    }



    private function _clean_tagparams()
    {
        if( !empty( ee()->TMPL->tagparams ) )
        {
            foreach( $this->tag_markers as $marker )
            {
                if( $key = array_search( $marker, ee()->TMPL->tagparams ) )
                {

                    // Replace 
                    if( $marker == 'CURRENT_URI' )
                    {
                        $marker = ee()->uri->uri_string();
                        if( $marker == '' ) $marker = '/';
                    }


                    if( $marker == 'CURRENT_URL' )
                    {
                        $marker = ee()->functions->fetch_current_uri();
                    }
                
                    ee()->TMPL->tagparams[ $key ] = $marker;
                }
            }
        }
    }


    private function _log( $message = '' )
    {
        if( SHORTLIST_DEBUG )
        {
            ee()->TMPL->log_item( 'Shortlist Debug: '. $message );
        }       
    }


    // --------------------------------------------------------------------
    // --------------------------------------------------------------------
    //
    // ACT Endpoints
    //
    // --------------------------------------------------------------------
    // --------------------------------------------------------------------

    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:add} tag
    * Passes off logic to the wrapper function
    */
    public function add_to_shortlist()
    {
        $this->shortlist_add_remove('add');
    }


    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:remove} tag
    * Passes off logic to the wrapper function
    */
    public function remove_from_shortlist()
    {
        $this->shortlist_add_remove('remove');
    }

    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:clone_list} tag
    * and also available within the view tag as {clone_url}
    */
    public function clone_shortlist()
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();

        // Pass over to the list model
        $state = ee()->shortlist_item_model->clone_list( $keys );

        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect

            // For the ajax calls we want to return the updated state of the
            // list count, and a new uri to allow the dev to use the action in 
            // for a toggle remove/add

            $list = ee()->shortlist_item_model->get_all(TRUE);
            $type = 'clone';
            $verb = 'cloned';

            $toggle_url = '';

            ee()->output->send_ajax_response(array(
                    'type'          => $type,
                    'list_items'    => $list,
                    'total_items'   => count( $list ),
                    'toggle_url'    => $toggle_url,
                    'verb'          => $verb
                ));

        }


        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }

    /* 
    * ACT Endpoint
    *
    * AJAX reordering helper. Accessed via the 
    * {exp:shortlist:reorder_uri} tag, and submitted directly
    * to via standard AJAX calls. Expects the lists data to be
    * a serialized array.
    */
    public function reorder_list()
    {
        $items = ee()->input->get_post('item');

        $state = FALSE;

        if( is_array( $items ) ) 
        {
            // Do the reorder
            $state = ee()->shortlist_item_model->reorder( $items );
            $this->_break_cache();
        }

        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect

            // For the ajax calls we want to return the updated state of the
            // list count, and a new uri to allow the dev to use the action in 
            // for a toggle remove/add
            $type = 'reorder';
            $verb = 'reordered';

            ee()->output->send_ajax_response(array(
                    'type'          => $type,
                    'verb'          => $verb,
                    'state'         => $state
                ));

            exit();

        }

        return;

    }


    


    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:add_list} tag
    * Creates a new clean list for a user
    */
    public function create_new_shortlist()
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();

        $keys = ee()->security->xss_clean( $keys );


        $make_default = TRUE;
        if( isset( $keys['make_default'] ) AND $keys['make_default'] == 'no' ) $make_default = FALSE;
        // Pass over to the list model
        $data = ee()->shortlist_list_model->create( $keys, $make_default);

        if( !is_array( $data ) ) return FALSE;

        $this->_break_cache();

        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect

            // For the ajax calls we want to return the updated state of the
            // list count, and a new uri to allow the dev to use the action in 
            // for a toggle remove/add
            $verb = 'created';
            $type = 'list_created';

            ee()->output->send_ajax_response(array(
                    'type'          => $type,
                    'list'          => $data,
                    'verb'          => $verb
                ));



        }

        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }

    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:list_edit_form} tag
    */
    public function shortlist_list_edit()
    {
        // We're looking to edit some list details
        // Gather the basic details first, authenticate,
        // then hand off to the list model for the heavy lifting

        $list_id = ee()->input->get_post('list_id');

        $list = ee()->shortlist_list_model->get( $list_id );

        if( empty($list) ) return;

        // Ok, carry on. Looks like a valid case.
        $ret = ee()->shortlist_list_model->edit_list( $list );

        $return = urldecode( ee()->input->get_post('ret') );
        ee()->functions->redirect( $return );
        exit();
    }

    /* 
    * ACT Endpoint
    *
    * Called from the {exp:shortlist:shortlist_set_default} tag
    * and also on edge cases where no default list is found as a fallback
    *
    * Accessible for the user as {make_default_url} within the 
    * {exp:shortlist:view} tag pair
    */
    public function make_shortlist_default()
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();
        $keys = ee()->security->xss_clean( $keys );

        // Pass over to the list model
        $state = ee()->shortlist_list_model->update_default( $keys );


        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect

            // For the ajax calls to make default, we just want to return
            // the success/failure state


            $verb = 'defaulted';

            // Also return a generic uri for setting a list as default
            // All a dev need do is append the list_id to this
            $generic_make_default_uri = $this->shortlist_set_default('toggle') . '&list_id=';
            ee()->output->send_ajax_response(array(
                    'type'          => 'make_default',
                    'verb'          => $verb,
                    'toggle'        => $generic_make_default_uri
                ));
            exit();


        }

        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }


    /* 
    * ACT Endpoint
    *
    * Called from the {remove_list_url} variable on the :lists tag pair
    */
    public function act_shortlist_remove_list()
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();
        $keys = ee()->security->xss_clean( $keys );

        // Pass over to the list model
        $list_id = ee()->shortlist_list_model->remove_list( $keys );

        $this->_break_cache();

        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect
            $verb = 'removed';

            ee()->output->send_ajax_response(array(
                    'type'          => 'remove_list',
                    'verb'          => $verb,
                    'list_id'       => $list_id));
            exit();


        }

        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }

    /* 
    * ACT Endpoint
    *
    * Called from the {clear_list_url} variable on the :lists tag pair
    */
    public function act_shortlist_clear_list()
    {
        $params = ee()->input->get_post('p');

        ee()->load->helper('string');
        $keys = unserialize( base64_decode( $params  ) );   

        if( !is_array( $keys ) ) exit();
        $keys = ee()->security->xss_clean( $keys );

        // Pass over to the list model
        $list_id = ee()->shortlist_list_model->clear_list( $keys );

        $this->_break_cache();

        if( $this->is_ajax_request() )
        {
            // This is an ajax call, don't redirect
            $verb = 'cleared';

            ee()->output->send_ajax_response(array(
                    'type'          => 'clear_list',
                    'verb'          => $verb,
                    'list_id'       => $list_id));
            exit();


        }

        $return = urldecode( ee()->input->get_post('ret') );

        ee()->functions->redirect( $return );
        exit();
    }




    /* 
    * Quick helper function that wraps tagdata
    * in an opening and closing form tags
    * and also adds some hidden fields while it's at it
    */
    private function _wrap_form( $act, $tagdata, $data = array())
    {
        $form_class = ee()->TMPL->fetch_param('form_class');
        $form_id    = ee()->TMPL->fetch_param('form_id');
        $form_class = $form_class != '' ? ' class="'.$form_class.'"' : '';
        $form_id    = $form_id != '' ? ' id="'.$form_id.'"' : '';
        $form_method = 'POST';

        $ret = $this->_get_ret_url();

        // Get the action_id 
        $action_url = $this->_get_action_url( $act );

        $hidden[] = '<input type="hidden" name="XID" value="{XID_HASH}"/>';
        $hidden[] = '<input type="hidden" name="ret" value="'.urlencode($ret).'"/>';
        $hidden[] = '<input type="hidden" name="list_id" value="'.$data['list_id'].'"/>';
        $bare = "<form name='shortlist_list_edit'". $form_class . $form_id ." method='".$form_method."' action='".$action_url."'>";

        $bare .= implode(' ',$hidden);

        $bare .= $tagdata;
        $bare .= "</form>";


        // Parse tagdata
        $t = ee()->TMPL->parse_variables(
                $bare,
                array($data),
                TRUE 
            );
            
        return $t;
    }

    /* 
    * Generates the return url for an ACT link
    * based on params and variables
    */
    private function _get_ret_url( $params = array() )
    {
        $ret = ee()->functions->fetch_current_uri();

        // Directly use the tagparams array as we're calling this dynamically
        // later on for toggle and this bypasses a logic hold
        if( isset( ee()->TMPL->tagparams['return'] ) )
        {
            $ret = ee()->TMPL->fetch_param('return');
            if( isset( $params['list_id'] ) AND strpos( $ret, '{this_list_id}' ) > 0 )
            {
                $ret = str_replace( '{this_list_id}', $params['list_id'], $ret );
            }
        }


        if( strpos( $ret, '{this_list_id}' ) > 0 )
        {
            $ret = str_replace( '{this_list_id}', '', $ret );
        }

        return $ret;
    }


    /** 
     * Breaks any caching for the lists if needed
     */
    private function _break_cache()
    {
        ee()->db->select( 'module_id' );
        ee()->db->where( 'module_name', 'Ce_cache' );
        $q = ee()->db->get( 'modules' );
        $ce_cache_installed = ( $q->num_rows() > 0 );
        $q->free_result();

        if ($ce_cache_installed) {
            if (!class_exists( 'Ce_cache_break' )) {
                include PATH_THIRD . 'ce_cache/libraries/Ce_cache_break.php';
            }

            $cache_break = new Ce_cache_break();
            $items = array();
            $tags = array('shortlist');

            $cache_break->break_cache( $items, $tags, false, 1 );
        }
    }
}

?>
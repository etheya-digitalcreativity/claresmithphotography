<?php if ( ! defined('EXT')) exit('No direct script access allowed');


// include_once config file
include_once PATH_THIRD.'shortlist/config'.EXT;

/**
 * Shortlist Extension Class
 *
 * @package         shortlist_ee_addon
 * @version         2.3.1
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel 
 */

class Shortlist_ext {

	// --------------------------------------------------------------------
	// PROPERTIES
	// --------------------------------------------------------------------

	/**
	 * Extension settings
	 *
	 * @access      public
	 * @var         array
	 */
	public $name 			= SHORTLIST_NAME;
	public $version 		= SHORTLIST_VERSION;
	public $description 	= 'Shortlist extension';
	public $settings_exist 	= FALSE;
	public $docs_url	 	= SHORTLIST_DOCS;
	public $class_name 		= SHORTLIST_CLASS_NAME;
	//public $required_by 	= array('module');

	/**
	 * Hooks used
	 *
	 * @access      private
	 * @var         array
	 */
	private $hooks = array(
		'channel_entries_row'
	);

	// --------------------------------------------------------------------
	// PUBLIC METHODS
	// --------------------------------------------------------------------

	/**
	 * PHP4 Constructor
	 *
	 * @see         __construct()
	 */
	public function Shortlist_ext($settings = FALSE)
	{
		$this->__construct($settings);
	}

	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @access      public
	 * @param       mixed     Array with settings or FALSE
	 * @return      null
	 */
	public function __construct($settings = FALSE)
	{
		//--------------------------------------------  
		//	Alias to get_instance()
		//--------------------------------------------
		if ( ! function_exists('ee') )
		{
			function ee()
			{
				return get_instance();
			}
		}

		// Set Class name
		$this->class_name = ucfirst(get_class($this));

		// Set settings
		$this->settings = $settings;

		// Define the package path
		ee()->load->add_package_path(PATH_THIRD.'shortlist');

		// Load helper
		ee()->load->helper('Shortlist_helper');

		// Load base model
		if( !isset( ee()->shorlist_model ) ) ee()->load->library('Shortlist_model');

		// Load other models
		if( !isset( ee()->shortlist_core_model ) ) Shortlist_model::load_models();
	}


	// --------------------------------------------------------------------
	// EXTENSION ACTIVATION, UPDATE AND DISABLING
	// --------------------------------------------------------------------

	/**
	 * Activate extension
	 *
	 * @access      public
	 * @return      null
	 */
	public function activate_extension()
	{
		// Loop through hooks and insert them in the DB
		foreach ($this->hooks AS $hook)
		{
			ee()->db->insert('extensions', array(
				'class'     => $this->class_name,
				'method'    => $hook,
				'hook'      => $hook,
				'priority'  => 10,
				'version'   => SHORTLIST_VERSION,
				'enabled'   => 'y'
			));
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Update extension
	 *
	 * @access      public
	 * @param       string    Saved extension version
	 * @return      null
	 */
	function update_extension($current = '')
	{
		if ($current == '' OR version_compare($current, SHORTLIST_VERSION) === 0)
		{
			return FALSE;
		}

		// init data array
		$data = array();

		// Add version to data array
		$data['version'] = SHORTLIST_VERSION;

		// Update records using data array
		ee()->db->where('class', $this->class_name);
		ee()->db->update('extensions', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * Disable extension
	 *
	 * @access      public
	 * @return      null
	 */
	function disable_extension()
	{
		// Delete records
		ee()->db->where('class', $this->class_name);
		ee()->db->delete('extensions');
	}

	public function channel_entries_tagdata( $tagdata, $row )
	{
		// has this hook already been called?
		if (isset(ee()->extensions->last_call) && ee()->extensions->last_call)
		{
			$tagdata = ee()->extensions->last_call;
		}

		return $tagdata;
	}


	public function channel_entries_row( &$that, $row )
	{
		// has this hook already been called?
		if (isset(ee()->extensions->last_call) && ee()->extensions->last_call)
		{
			$row = ee()->extensions->last_call;
		}

		if( isset( $that->is_shortlist ) )
		{
			// This is a shortlist row
			// Get this item's details for this user
			// and add the data as tag data for parsing

			
			$items = $that->shortlist_items;
			$item = array();

			foreach( $items as $item_row )
			{
				if( $item_row['entry_id'] == $row['entry_id'] ) $item = $item_row;
			}

			/*if( empty( $item ) ) 
			{
				// Just in case
				$shortlist = new Shortlist();
				$item = ee()->shortlist_item_model->get_one( $row['entry_id'], 'entry_id' );
				// Add the action urls as appropriate
				$item['add_url'] = $shortlist->shortlist_add( $item['item_id'] );
				$item['remove_url'] = $shortlist->shortlist_remove( $item['item_id'] );

			}*/

			// Is this an externally sourced entry?
			if( $row['channel_name'] != SHORTLIST_CHANNEL_NAME )
			{
				$item['is_external'] = FALSE;
			}
			else
			{
				$item['is_external'] = TRUE;

				// We need to break up the datablock field for usage in this item
				// Do we need to get the datablock/unique field ids?
				$unique_field_id = $that->shortlist_fields[ SHORTLIST_UNIQUE_FIELD_NAME ];
				$datablock_field_id = $that->shortlist_fields[ SHORTLIST_DATABLOCK_FIELD_NAME ];

				$datablock = unserialize( base64_decode( $row[ 'field_id_'. $datablock_field_id] ) );

				if( is_array( $datablock ) ) 
				{
					$item = array_merge( $item, $datablock );
				}
			}

			// Add our 'added' date to override the entry_date
			if( isset( $item['added'] ) ) $row['entry_date'] = $item['added'];

			$row = array_merge( $row, $item );
		}
		
		return $row;
	}


	// --------------------------------------------------------------------


	public function shortlist_remove( $item_id = 0)
	{
		$act_url = $this->_get_action_url( 'add_to_shortlist' );

		// Take any and all params and wrap them up neatly
		ee()->load->helper('string');

		if( $item_id == 0 ) $params = ee()->TMPL->tagparams;
		else $params['item_id'] = $item_id;

		$params = base64_encode( serialize( $params ) );

		$act_url .= '&p=' . $params;
		$act_url .= '&ret=' . urlencode( ee()->functions->fetch_current_uri() );

		return $act_url;
	}


	// --------------------------------------------------------------------

	public function shortlist_add()
	{
		$act_url = $this->_get_action_url( 'remove_from_shortlist' );

		// Take any and all params and wrap them up neatly
		ee()->load->helper('string');
		$params = base64_encode( serialize(ee()->TMPL->tagparams) );

		$act_url .= '&p=' . $params;
		$act_url .= '&ret=' . urlencode( ee()->functions->fetch_current_uri() );
		return $act_url;
	}

	// --------------------------------------------------------------------


	private function _get_action_url($method_name)
	{
		$action_id 	= ee()->db->where(
			array(
				'class' 	=> SHORTLIST_CLASS_NAME, 
				'method' 	=> $method_name
			)
		)->get('actions')->row('action_id'); 

		return ee()->functions->fetch_site_index(0, 0) . '?ACT=' . $action_id;
	}
	//END get_action_url



}

<?php defined('BASEPATH') or exit('No direct script access allowed');

$lang = array(

	'shortlist_module_name'			=> 'Shortlist',
	'shortlist_module_description'	=> 'Listen - ohne Wenn und Aber',

	'unique_val_field_label' 	=> 'Eindeutiger Wert',
	'unique_val_field_desc'	=> 'Eindeutiger Wert dieses Eintrags beim Hinzufügen zu einer Liste',

	'datablock_field_label'	=> 'Datenblock',
	'datablock_field_desc'	=> 'Vollständige Daten dieses Eintrags in Form eines Array',


	// CP Titles
	'shortlist_item_page' => 'Eintrag',
	'shortlist_list_page' => 'Liste',
	'shortlist' => 'Shortlist',
	'settings' => 'Einstellungen',

	'shortlist_title' 		=> 'Titel',
	'shortlist_Internal' 	=> 'Intern',
	'shortlist_External'	=> 'Extern',


	  'period_year' => 'Jahr',
	  'period_years' => 'Jahre',
	  'period_month' => 'Monat',
	  'period_months' => 'Monate',
	  'period_day' => 'Tag',
	  'period_days' => 'Tage',
	  'period_hour' => 'Stunde',
	  'period_hours' => 'Stunden',
	  'period_min' => 'Minute',
	  'period_mins' => 'Minuten',
	  'period_sec' => 'Sekunde',
	  'period_secs' => 'Sekunden',
	  //'period_postfix' => 'n',
	  'period_ago' => 'vor',
	  'period_now' => 'in diesem Moment',
	  // alternative: 'period_now' => 'jetzt',


	'shortlist_list_item_single' => 'Gelisteter Inhalt',
	'shortlist_list_item_plural' => 'Gelistete Inhalte',

	'shortlist_item_single'			=> 'Eintrag',
	'shortlist_item_plural'			=> 'Einträge',

	'shortlist_user_list_single'	=> 'Benutzerliste',
	'shortlist_user_list_plural'	=> 'Benutzerlisten',

	'shortlist_list_single'			=> 'Liste',
	'shortlist_list_plural'			=> 'Listen',

	'shortlist_internal'			=> 'intern',
	'shortlist_external'			=> 'extern',

	'shortlist_user_or_guest'		=> 'Ersteller der Liste',
	'shortlist_last_active'			=> 'Letzte Aktivität',
	'shortlist_guest'				=> 'Gast',

	'shortlist_details_about_this_item'	=> 'Einzelheiten über diesen Eintrag',
	'shortlist_view_entry'				=> 'Eintrag anzeigen',
	'shortlist_external_item_details'	=> 'Einzelheiten über externen Eintrag',
	'shortlist_unique_marker'			=> 'Eindeutiger Marker',
	'shortlist_item_appears_in'			=> 'Dieser Eintrag befindet sich auf',

	'shortlist_details_about_this_list'	=> 'Einzelheiten über diese Liste',
	'shortlist_owned_by'				=> 'Erstellt von',
	'shortlist_share_id'				=> 'Share ID',
	'shortlist_list_has'				=> 'Diese Liste hat',
	'shortlist_added'					=> 'Eintrag hinzugefügt',
	'shortlist_items_have_been_cloned'	=> 'Die Einträge dieser Liste wurden geklont',
	'shortlist_times_single'			=> 'mal',
	'shortlist_times_plural'			=> 'mal',
	'shortlist_cloned_by'				=> 'Geklont',
	'shortlist_when'					=> 'Wann',


	'count'							=> 'Auf wie vielen Listen?',
	'type'							=> 'Inhalts-Typ',
	'item_count'					=> 'Anzahl der gelisteten Einträge',

	'' => ''
);
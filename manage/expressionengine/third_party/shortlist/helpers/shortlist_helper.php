<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Shortlist helper class
 *
 * @package         shortlist_ee_addon
 * @version         2.3.1
 * @author          Joel Bradbury ~ <joel@squarebit.co.uk>
 * @link            http://squarebit.co.uk/shortlist
 * @copyright       Copyright (c) 2013, Joel 
 */

// --------------------------------------------------------------------

/**
 * Object to Array
 *
 * From a multi-dimensional object return a 
 * usable multi-dimensional array
 *
 * @param      array
 * @param 	   bool 
 * @return     array
 */
if ( ! function_exists('Shortlist_obj_to_array'))
{
	function Shortlist_obj_to_array($obj, $clean = FALSE, $convert = array() ) 
	{

	    if(is_object($obj)) $obj = (array) $obj;

	    if(is_array($obj)) {

	        $new = array();

	        foreach($obj as $key => $val) {

	        	if( $clean ) 
	        	{
		        	$key = str_replace( '-', '_', $key );

		        	if( isset( $convert[ $key ] ) ) $key = $convert[ $key ];
		        }

	            $new[$key] = Shortlist_obj_to_array($val, $clean);
	        }
	    }
	    else $new = $obj;

	    return $new;
	}
}

/**
 * Plural helper
 *
 * @param       int
 * @return      string
 */
if ( ! function_exists('lang_plural'))
{
	function lang_plural($count)
	{
		if( $count == 0 ) return 's';
		elseif( $count == 1 ) return '';
		else return 's';
	}
}


/**
 * Plural helper
 *
 * @param       int
 * @return      string
 */
if ( ! function_exists('lang_switch'))
{
	function lang_switch($lang, $count)
	{
		if( $count == 0 ) return lang( $lang . '_plural');
		elseif( $count == 1 ) return lang( $lang . '_single');
		else return lang( $lang . '_plural');
	}
}
/**
 * Debug
 *
 * @param       mixed
 * @param       bool
 * @return      void
 */
if ( ! function_exists('dumper'))
{
	function dumper($var, $exit = TRUE)
	{
		echo '<pre>'.print_r($var, TRUE).'</pre>';

		if ($exit) exit;
	}
}

// --------------------------------------------------------------

/* End of file Shortlist_helper.php */
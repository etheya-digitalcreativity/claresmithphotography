<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Wyvern Video Fieldtype Class
 *
 * @package     ExpressionEngine
 * @subpackage  Fieldtypes
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';
 
class Wyvern_video_helper {

    // Attributes for each video
    public $attributes = array(
        'id' => '',
        'url' => '',
        'width' => '360',
        'height' => '240',
        'type' => '',
        'title' => '',
        'description' => '',
        'thumbnail_small' => '',
        'thumbnail_large' => '',
        'author' => '',
        'length' => '',
        'embed' => '' // Full iframe embed code
    );

    public $vimeo_key = '08a7668f9e10bcc2e8bbf58220566ed7';
    public $vimeo_secret = '59294c10bbabab55';

    // Defaults
    public $default_settings = array(
        'settings_global' => array(
            'global_width' => '360',
            'global_height' => '240'
        ),
        'settings_youtube' => array(
            'user' => '',
            'show_on_load' => 'n'
        ),
        'settings_vimeo' => array(
            'user' => '',
            'show_on_load' => 'n'
        )
    );

    public $cache;
    public $site_url;

    function __construct()
    {
        $this->EE =& get_instance();
        
        // Create cache
        if (! isset($this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME];

        $this->site_url = $this->EE->config->slash_item('site_url');
        $this->site_id = $this->EE->config->item('site_id');

        // For PHP < 5.2 support
        require_once PATH_THIRD .'wyvern_video/libraries/jsonwrapper/jsonwrapper.php';
    }

    public function load_assets()
    {
        if (! isset($this->cache['assets_added']))
        {
            $this->EE->load->library('javascript');

            // Get the base themes/third_party/ path
            $theme_path = $this->_get_theme_folder_path();
            $theme_url = $this->_get_theme_folder_url();

            $cp_theme  = $this->EE->config->item('cp_theme'); 
            $cp_theme_url = $this->EE->config->slash_item('theme_folder_url').'cp_themes/'.$cp_theme.'/';

            $load_vimeo_url = $this->_get_site_index() . '?ACT='. $this->EE->cp->fetch_action_id('wyvern_video', 'load_vimeo');

            $this->EE->cp->add_to_head('
                <link rel="stylesheet" href="'. $theme_url .'wyvern_video/styles/dialog.css" />
                <link rel="stylesheet" href="'. $theme_url .'wyvern_video/styles/field.css" />
            ');

            // Append to the current wyvern_config object if it exists
            $script = '
            if (typeof window.wyvern_config == "undefined") {
                window.wyvern_config = {};
            }

            wyvern_config.cp_global_images = "'. $this->EE->config->slash_item('theme_folder_url').'cp_global_images/' .'";
            wyvern_config.cp_theme_url = "'. $cp_theme_url .'";
            wyvern_config.load_vimeo_url = "'. $load_vimeo_url .'";
            wyvern_config.wyvern_video = '.json_encode($this->get_settings());

            $this->EE->cp->add_to_head('
                <script type="text/javascript">'. $script .'</script>
                <script type="text/javascript" src="'. $theme_url .'wyvern_video/scripts/wyvern_video_dialog.js"></script>
            ');

            $this->EE->cp->add_to_foot('<div id="wyvern-video-dialog-jquery"><div id="wyvern-video-dialog-contents-jquery"></div></div>');

            // Load Assets' assets
            if ( ! isset($this->EE->session->cache['assets']['included_sheet_resources']) 
                AND array_key_exists('assets', $this->EE->addons->get_installed()))
            {
                if (! class_exists('Assets_helper'))
                {
                    require PATH_THIRD.'assets/helper.php';
                }

                $assets_helper = new Assets_helper;
                $assets_helper->include_sheet_resources();
            }

            $this->cache['assets_added'] = true;
        }
    }

    // This needs to be loaded separately so it is included AFTER the Matrix class 
    // is include, otherwise our callback in matrix2.js won't be bound.
    public function load_js_matrix()
    {
        if(!isset($this->cache['js_added_matrix']))
        {
            $this->EE->cp->add_to_foot('<script type="text/javascript" src="'. $this->_get_theme_url() .'scripts/matrix2.js"></script>');
        }
        
        $this->cache['js_added_matrix'] = true;
    }

    public function get_field_settings_options($obj, $settings, $matrix = false, $prefix = false)
    {
        $this->EE->lang->loadfile('wyvern_video');
        $global_settings = $this->get_settings();

        // We're either editing a Wyvern field, or Wyvern Video field because this can be called from Wyvern.
        $settings = isset($settings['wyvern_video']) ? $settings['wyvern_video'] : $settings;
        $name = $prefix ? 'wyvern[wyvern_video]' : 'wyvern_video';

        /*
            Resize field?
        */

        $details = form_checkbox(array(
                        'name'      => $name.'[show_details]',
                        'id'        => 'show_details',
                        'value'     => 'yes',
                        'checked'   => (isset($settings['show_details']) AND $settings['show_details'] == 'no') ? false : true
                    )) .' <label for ="show_details">Yes</label>';

        $width  = form_input($name.'[field_width]', (isset($settings['field_width']) AND $settings['field_width'] != '') ? $settings['field_width'] : $global_settings['settings_global']['global_width'], 'style="width: 50px"');
        $height = form_input($name.'[field_height]', (isset($settings['field_height']) AND $settings['field_height'] != '') ? $settings['field_height'] : $global_settings['settings_global']['global_height'], 'style="width: 50px"');
        
        $lock   = form_checkbox(array(
                        'name'      => $name.'[allow_resize]',
                        'id'        => 'allow_resize',
                        'value'     => 'yes',
                        'checked'   => (isset($settings['allow_resize']) AND $settings['allow_resize'] == 'yes') ? true : false
                    )) .' <label for ="allow_resize">Yes</label>';

        // What sort of fieldtype is requesting these settings?
        $is_wyvern_video_field = get_class($obj) == 'wyvern_video_ft' ? true : false;

        /*
            If we're displaying settings in a Matrix field
        */
        if ($matrix)
        {
            $html  = '<table class="matrix-col-settings">';

            if ($is_wyvern_video_field)
            {
                $html .= '<tr class="matrix-first odd">
                        <th class="matrix-first" style="width: 40%">'. lang('show_details') .'</td>
                        <td class="matrix-last" style="width: 40%">'. $details .'</td>
                      </tr>
                      <tr class="matrix-first even">
                        <th class="matrix-first">'. lang('field_width') .'</td>
                        <td class="matrix-last">'. $width .'</td>
                      </tr>
                      <tr class="matrix odd">
                        <th class="matrix-first">'. lang('field_height') .'</th>
                        <td class="matrix-last">'. $height .'</td>
                      </tr>
                      <tr class="matrix even">
                        <th class="matrix-first">'. lang('allow_resize') .'</th>
                        <td class="matrix-last">'. $lock .'</td>
                      </tr>';
            }
            else
            {
                $html .= '<tr class="matrix-first odd">
                        <th class="matrix-first" style="width: 40%">'. lang('field_width') .'</td>
                        <td class="matrix-last" style="width: 40%">'. $width .'</td>
                      </tr>
                      <tr class="matrix even">
                        <th class="matrix-first">'. lang('field_height') .'</th>
                        <td class="matrix-last">'. $height .'</td>
                      </tr>
                      <tr class="matrix odd">
                        <th class="matrix-first">'. lang('allow_resize') .'</th>
                        <td class="matrix-last">'. $lock .'</td>
                      </tr>';
            }


            $html .= '</table>';

            $return = $html;
        }
        
        /*
            Normal settings display
        */
        else
        {
            if ($is_wyvern_video_field)
            {
                $return = array(
                    array('<strong>'. lang('show_details') .'</strong>', $details),
                    array('<strong>'. lang('field_width') .'</strong>', $width),
                    array('<strong>'. lang('field_height') .'</strong>', $height),
                    array('<strong>'. lang('allow_resize') .'</strong>', $lock)
                );
            }
            else
            {
                $return = array(
                    array('<strong>'. lang('field_width') .'</strong>', $width),
                    array('<strong>'. lang('field_height') .'</strong>', $height),
                    array('<strong>'. lang('allow_resize') .'</strong>', $lock)
                );
            }
        }
        
        return $return;
    }

    public function get_iframe($data)
    {
        if (isset($data->type))
        {
            switch($data->type)
            {
                case 'youtube':
                    return '<iframe src="http://www.youtube.com/embed/'. $data->id .'?rel=0&wmode=opaque" border="0" height="'. $data->height .'" width="'. $data->width .'"></iframe>';
                break;

                case 'vimeo':
                    return '<iframe src="http://player.vimeo.com/video/'. $data->id .'?title=0&byline=0&portrait=0" border="0" height="'. $data->height .'" width="'. $data->width .'"></iframe>';
                break;
            }
        }

        return '';
    }

    /*
        Get module settings (not individual field settings)
    */
    public function get_settings()
    {
        // This is called during the install process before this table is created, but during
        // install we don't actually need settings, so just make sure the table exists before 
        // proceeding.
        if ( ! isset($this->cache['settings']) AND $this->EE->db->table_exists('boldminded_settings'))
        {
            // So the upgrade from 1.1 to 1.2 won't bomb out when they go to the publish page before running the upgrade.
            if (version_compare(WYVERN_VIDEO_VERSION, '1.2', '<'))
            {
                $row = $this->EE->db->select('settings')
                                    ->where('addon', WYVERN_VIDEO_SHORT_NAME)
                                    ->get('boldminded_settings')
                                    ->row('settings');

                $settings = unserialize($row);
            }
            else
            {
                $row = $this->EE->db->select('settings')
                                    ->where('addon', WYVERN_VIDEO_SHORT_NAME)
                                    ->where('site_id', $this->site_id)
                                    ->get('boldminded_settings')
                                    ->row('settings');

                if (empty($row))
                {
                    $settings = array();
                }
                else
                {
                    $settings = $this->object_to_array(json_decode($row));
                }
            }

            $this->cache['settings'] = ! empty($settings) ? $settings : $this->default_settings;
        }

        return isset($this->cache['settings']) ? $this->cache['settings'] : array();
    }

    public function get_setting_field($name, $value, $group = FALSE)
    {
        $type = 'text';

        if ( isset($this->default_settings[$group][$name]) AND in_array($this->default_settings[$group][$name], array('y', 'n')))
        {
            $type = 'boolean';
            $value = $value ? $value : $this->default_settings[$group][$name];
        }

        switch($type)
        {
            case 'boolean':
                $field = form_checkbox(array(
                    'id' => $name,
                    'name' => 'setting['. $group .']['. $name .']', 
                    'checked' => ($value == 'y' ? TRUE : FALSE),
                    'value' => 'y'));
            break;

            default:
                $field = form_input(array(
                    'id' => $name,
                    'name' => 'setting['. $group .']['. $name .']', 
                    'style' => 'width: 98%',
                    'value' => $value));
            break;
        }

        return $field;
    }

    public function save_settings($data)
    {
        $data = array(
            'site_id'   => $this->site_id,
            'addon'     => WYVERN_VIDEO_SHORT_NAME,
            'settings' => json_encode($data)
        );

        $where = array(
            'site_id'   => $this->site_id,
            'addon'     => WYVERN_VIDEO_SHORT_NAME
        );

        $this->insert_or_update('boldminded_settings', $data, $where, 'id');
    }

    public function get_rte_fields()
    {
        $qry = $this->EE->db->select('field_id, field_settings, field_type')
                            ->where_in('field_type', array('textarea', 'wyvern', 'wygwam'))
                            ->where('site_id', $this->EE->config->item('site_id'))
                            ->get('channel_fields');

        $rte_fields = array();

        foreach ($qry->result() as $row)
        {
            $field_settings = unserialize(base64_decode($row->field_settings));

            if ((isset($field_settings['field_enable_rte']) AND $field_settings['field_enable_rte'] == 'y') OR $row->field_type == 'wyvern')
            {
                $rte_fields[] = 'field_id_'.$row->field_id;
            }
        }

        return $rte_fields;
    }

    /*
    @param - string
    @param - array of data to be inserted, key => value pairs
    @param - array of data used to find the row to update, key => value pairs

    _insert_or_update('some_table', array('foo' => 'bar'), array('id' => 1, 'something' => 'another-thing'))

    */
    public function insert_or_update($table, $data, $where, $primary_key = 'entry_id')
    {
        $query = $this->EE->db->get_where($table, $where);

        // No records were found, so insert
        if ($query->num_rows() == 0)
        {
            $this->EE->db->insert($table, $data);
            return $this->EE->db->insert_id();
        }
        // Update existing record
        elseif ($query->num_rows() == 1)
        {
            $this->EE->db->where($where)->update($table, $data);
            return $this->EE->db->select($primary_key)->from($table)->where($where)->get()->row($primary_key);
        }
    }

    /**
     * Convert an obj to an array so EE's parse_variables method accepts it.
     *
     * @param object $obj The PHP data object to transform
     */
    public function object_to_array($obj)
    {
        if(is_object($obj)) $obj = (array) $obj;

        if(is_array($obj)) 
        {
            $new = array();
            foreach($obj as $key => $val) 
            {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else 
        {
            $new = $obj;
        }

        return $new;
    }

    /**
     * Searches a string for an HTML element and grabs the attributes
     *
     * @access  public
     * @param   string
     * @param   regex
     * @return  array($args, $full_tag_match)
     */ 
    public function get_args($value, $regex)
    {
        preg_match($regex, $value, $matches);
        $raw_tag = isset($matches[0]) ? $matches[0] : '';
        $tag = str_replace(array("\r\n", "\r", "\n", "\t"), " ", $raw_tag);
        return array(trim((preg_match("/\s+.*/", $tag, $matches))) ? $raw_tag : '', $raw_tag);
    }

    /**
     * Return parameters as an array
     *
     * Creates an associative array from a string
     * of parameters: sort="asc" limit="2" etc.
     *
     * @access  public
     * @param   string
     * @return  bool
     */ 
    public function assign_parameters($str)
    {
        if ($str == "")
            return FALSE;
                        
        // \047 - Single quote octal
        // \042 - Double quote octal
        
        // I don't know for sure, but I suspect using octals is more reliable than ASCII.
        // I ran into a situation where a quote wasn't being matched until I switched to octal.
        // I have no idea why, so just to be safe I used them here. - Rick
        
        // matches[0] => attribute and value
        // matches[1] => attribute name
        // matches[2] => single or double quote
        // matches[3] => attribute value
        preg_match_all("/(\S+?)\s*=\s*(\042|\047)([^\\2]*?)\\2/is",  $str, $matches, PREG_SET_ORDER);

        if (count($matches) > 0)
        {
            $result = array();
        
            foreach($matches as $match)
            {
                $result[$match[1]] = (trim($match[3]) == '') ? $match[3] : trim($match[3]);
            }

            return $result;
        }

        return FALSE;
    }

    public function watermark($source = FALSE, $width = 360, $height = 240)
    {
        $config['source_image'] = $source;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = $this->_get_theme_url() .'images/vimeo-overlay.png';
        $config['wm_opacity'] = '50';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'center';

        $this->EE->load->library('image_lib'); 
        $this->EE->image_lib->initialize($config); 
        $this->EE->image_lib->watermark();
    }

    private function _get_theme_url()
    {
        return $this->_get_theme_folder_url().'wyvern_video/';
    }
    
    private function _get_theme_folder_url()
    {
        return URL_THIRD_THEMES;
    }
    
    private function _get_theme_folder_path()
    {
        return PATH_THEMES . 'third_party/';
    }

    public function _get_site_index()
    {
        $site_index = $this->EE->config->item('site_index');
        $index_page = $this->EE->config->item('index_page');
        
        $index = ($site_index != '') ? $site_index : (($index_page != '') ? $index_page : '');

        return $this->EE->functions->remove_double_slashes($this->EE->config->slash_item('site_url') . $index);
    }

}
<?php

if (! defined('WYVERN_VIDEO_VERSION'))
{
    define('WYVERN_VIDEO_VERSION', '1.2.7');
    define('WYVERN_VIDEO_NAME', 'Wyvern Video');
    define('WYVERN_VIDEO_RTE_NAME', 'Wyvern_video_rte');
    define('WYVERN_VIDEO_EXT_NAME', 'Wyvern_video_ext');
    define('WYVERN_VIDEO_SHORT_NAME', 'wyvern_video');
    define('WYVERN_VIDEO_DESCRIPTION', 'Video management your way.');
}

$config['name'] = WYVERN_VIDEO_NAME;
$config['short_name'] = WYVERN_VIDEO_SHORT_NAME;
$config['version'] = WYVERN_VIDEO_VERSION;
$config['description'] = WYVERN_VIDEO_DESCRIPTION;
$config['docs_url'] = 'http://boldminded.com/add-ons/wyvern-video';
$config['nsm_addon_updater']['versions_xml'] = 'http://boldminded.com/versions/wyvern-video';
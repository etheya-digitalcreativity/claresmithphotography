<?php

/**
 * ExpressionEngine Wyvern Video Module Class
 *
 * @package     ExpressionEngine
 * @subpackage  Modules
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';

class wyvern_video {

    public $settings;
    private $params;
    private $cache_path;
    private $cache;
    private $service;

    public function __construct()
    {
        $this->EE =& get_instance();
        
        // Create cache
        if (! isset($this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME]))
        {
            $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME] = array();
        }
        $this->cache =& $this->EE->session->cache[WYVERN_VIDEO_SHORT_NAME];
        
        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }

        // $this->EE->load->library('pagination');
        // $this->pagination = new Pagination_object(__CLASS__);
        // $this->pagination->dynamic_sql = FALSE;
        // $this->pagination->get_template();

        $this->settings = $this->EE->wyvern_video_helper->get_settings();

        $this->cache_path = APPPATH . 'cache/wyvern_video/';

        // Make sure our cache folder exists.
        if ( ! is_dir($this->cache_path))
        {
            mkdir($this->cache_path, DIR_WRITE_MODE, TRUE);
        }
    }

    private function _get_params()
    {
        $this->params['query']      = $this->EE->TMPL->fetch_param('query');
        $this->params['settings']   = $this->settings['settings_'. $this->service];
        $this->params['username']   = $this->EE->TMPL->fetch_param('user', $this->params['settings']['user']);
        $this->params['prefix']     = $this->EE->TMPL->fetch_param('variable_prefix', '');

        $this->params['width']      = $this->EE->TMPL->fetch_param('width', 360);
        $this->params['height']     = $this->EE->TMPL->fetch_param('height', 240);

        $this->params['limit']      = $this->EE->TMPL->fetch_param('limit', 20);
        $this->params['offset']     = $this->EE->TMPL->fetch_param('offset', 1);
        // $this->params['page']       = $this->EE->TMPL->fetch_param('page');

        // if ($this->params['page'])
        // {
            
        // }

        // $this->pagination->build();
    }

    /**
     * The template module tag for displaying a list of videos from YouTube based on a query.
     *
     * {exp:wyvern_video:youtube}
     *      {video_id}
     *      {title}
     *      {content} or {description}
     *      {url}
     *      {url_mobile}
     *      {thumbnail_0} {thumbnail_1} {thumbnail_2} {thumbnail_3}
     *      {author}
     *      {category}
     *      {keywords}
     *          {keyword}
     *      {/keywords}
     *      {duration}
     *      {view_count}
     *      {favorite_count}
     *      {id} - e.g. http://gdata.youtube.com/feeds/api/videos/[video id]
     *      {published}
     *      {updated}
     *      {embed}
     * {/exp:wyvern_video:youtube}
     */
    public function youtube()
    {
        $videos = array();
        $this->service = 'youtube';
        $this->_get_params();

        // Avoid conflict with the REST module
        if ( ! isset($this->EE->curl))
        {
            $this->EE->load->library('curl');
        }

        if ($this->params['username'])
        {
            $url = 'http://gdata.youtube.com/feeds/api/users/'. $this->params['username'] .'/uploads?q='. $this->params['query'] .'&max-results='. $this->params['limit'] .'&start-index='. $this->params['offset'];
        }
        else
        {
            $url = 'http://gdata.youtube.com/feeds/api/videos?q='. $this->params['query'] .'&max-results='. $this->params['limit'];
        }

        $this->EE->TMPL->log_item('Wyvern Video: Querying '. $url);
        $this->params['url'] = $url;
        $prefix = $this->params['prefix'];
        
        try 
        {
            if ( ($query = $this->_get_cache()) !== TRUE)
            {
                $query = $this->EE->curl->simple_get($url);
                $this->_set_cache($query);
            }

            $data = simplexml_load_string($query);

            if ( ! $data OR count($data->entry) == 0)
            {
                $this->EE->TMPL->log_item('Wyvern Video: No Results found for '. $url);
                return $this->EE->TMPL->no_results();
            }

            $k = 0;
            foreach($data->entry as $entry)
            {
                $videos[$k][$prefix.'title'] = (string) $entry->title;
                $videos[$k][$prefix.'author'] = (string) $entry->author->name;
                $videos[$k][$prefix.'description'] = (string) $entry->content;
                $videos[$k][$prefix.'content'] = (string) $entry->content;
                $videos[$k][$prefix.'published'] = strtotime($entry->published);
                $videos[$k][$prefix.'updated'] = strtotime($entry->updated);

                // get nodes in media: namespace for media information
                $media = $entry->children('http://search.yahoo.com/mrss/');

                // get video player URL
                $attrs = $media->group->player->attributes();

                $videos[$k][$prefix.'url'] = str_replace('&feature=youtube_gdata_player', '', (string) $attrs['url']);
                $videos[$k][$prefix.'url_mobile'] = str_replace('www.youtube.com', 'm.youtube.com', $videos[$k][$prefix.'url']);
                $videos[$k][$prefix.'video_id'] = str_replace('http://www.youtube.com/watch?v=', '', $videos[$k][$prefix.'url']);
                $videos[$k][$prefix.'id'] = $videos[$k][$prefix.'video_id'];
                $videos[$k][$prefix.'category'] = (string) $media->group->category;

                $keywords = explode(',', (string) $media->group->keywords);

                foreach ($keywords as $keyword)
                {
                    $videos[$k][$prefix.'keywords'][] = array($prefix.'keyword' => trim($keyword));
                }

                // get video thumbnails
                $i = 0;
                foreach ($media->group->thumbnail as $thumb)
                {
                    $attrs = $thumb->attributes();
                    $videos[$k][$prefix.'thumbnail_'.$i] = (string) $attrs['url'];
                    $i++;
                }

                // get <yt:duration> node for video length
                $yt = $media->children('http://gdata.youtube.com/schemas/2007');
                $attrs = $yt->duration->attributes();

                $videos[$k]['duration'] = (string) $attrs['seconds'];

                // get <yt:stats> node for viewer statistics
                $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
                if ($yt->statistics AND ($attrs = $yt->statistics->attributes()))
                {
                    $videos[$k][$prefix.'favorite_count'] = isset($attrs['favoriteCount']) ? (string) $attrs['favoriteCount'] : '';
                    $videos[$k][$prefix.'view_count'] = isset($attrs['viewCount']) ? (string) $attrs['viewCount'] : '';
                }
                else
                {
                    $videos[$k][$prefix.'favorite_count'] = '';
                    $videos[$k][$prefix.'view_count'] = '';
                }

                // finally add the full embed code...
                $videos[$k][$prefix.'embed'] = '<iframe src="http://www.youtube.com/embed/'. $videos[$k][$prefix.'video_id'] .'?rel=0&wmode=opaque&hd=1" border="0" width="'. $this->params['width'] .'" height="'. $this->params['height'] .'"></iframe>';

                $k++;
            }

            // if ($this->pagination->enabled == TRUE)
            // {
            //     $this->EE->TMPL->tagdata = $this->pagination->render($this->EE->TMPL->tagdata);
            // }
        }
        catch (Exception $e) 
        {
            $this->EE->TMPL->log_item('Wyvern Video: Could not load '. $this->params['url']);
        }

        return $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $videos);
    }


    /**
     * The template module tag for displaying a list of videos from Vimeo based on a query.
     *
     * {exp:wyvern_video:vimeo}
     *      {video_id}
     *      {title}
     *      {content} or {description}
     *      {url}
     *      {url_mobile}
     *      {thumbnail_0} {thumbnail_1} {thumbnail_2} {thumbnail_3}
     *      {author}
     *      {tags}
     *          {tag}
     *      {/tags}
     *      {duration}
     *      {view_count}
     *      {favorite_count}
     *      {id}
     *      {published}
     *      {updated}
     *      {embed}
     * {/exp:wyvern_video:vimeo}
     */
    public function vimeo()
    {
        $videos = array();
        $this->service = 'youtube';
        $this->_get_params();

        $url = 'http://vimeo.com/api/rest/v2';

        // The URL is not actually used in this class for Vimeo, 
        // the Vimeo lib handles it, but adding here for logging purposes.
        if ($this->params['username'])
        {

            $url .= $this->params['username'] .'?user_id='. $this->params['username'] .'&query='. $this->params['query'] .'&per_page='. $this->params['limit'] .'&page='. $this->params['offset'];
        }
        else
        {
            $url .= $this->params['username'] .'?query='. $this->params['query'] .'&per_page='. $this->params['limit'] .'&page='. $this->params['offset'];
        }

        $this->EE->TMPL->log_item('Wyvern Video: Querying '. $url);
        $this->params['url'] = $url;
        $prefix = $this->params['prefix'];

        try 
        {
            $data = $this->load_vimeo('videos.search', array(
                'per_page'  => $this->params['limit'],
                'page'      => $this->params['offset'],
                'query'     => $this->params['query'],
                'user_id'   => $this->params['username']
            ), TRUE);

            if ( ! $data OR count($data->videos->video) == 0)
            {
                return $this->EE->TMPL->no_results();
            }

            foreach ($data->videos->video as $k => $entry)
            {
                $videos[$k][$prefix.'title'] = $entry->title;

                // get the video details/info
                $video_info = $this->load_vimeo('videos.getInfo', array(
                    'video_id'   => $entry->id
                ), TRUE);

                $video = $video_info->video[0];

                // get basic info
                $videos[$k][$prefix.'id']               = $video->id;
                $videos[$k][$prefix.'video_id']         = $video->id;
                $videos[$k][$prefix.'content']          = $video->description;
                $videos[$k][$prefix.'description']      = $video->description;
                $videos[$k][$prefix.'published']        = strtotime($video->upload_date);
                $videos[$k][$prefix.'updated']          = strtotime($video->modified_date);
                $videos[$k][$prefix.'view_count']       = $video->number_of_plays;
                $videos[$k][$prefix.'favorite_count']   = $video->number_of_likes;
                $videos[$k][$prefix.'duration']         = $video->duration;
                $videos[$k][$prefix.'author']           = $video->owner->display_name;

                foreach ($video->tags->tag as $tag)
                {
                    $videos[$k][$prefix.'tags'][] = array($prefix.'tag' => $tag->normalized);
                }

                // get video thumbnails
                foreach ($video->thumbnails->thumbnail as $thb_key => $thumbnail)
                {
                    $videos[$k][$prefix.'thumbnail_'. $thb_key] = $thumbnail->_content;
                }

                // get the full URLs
                foreach ($video->urls->url as $vid_key => $url)
                {
                    if ($url->type == 'mobile')
                    {
                        $videos[$k][$prefix.'url_mobile'] = $url->_content;
                    }
                    else
                    {
                        $videos[$k][$prefix.'url'] = $url->_content;
                    }
                }

                // finally add the full embed code...
                $videos[$k][$prefix.'embed'] = '<iframe src="http://player.vimeo.com/video/'. $videos[$k][$prefix.'video_id'] .'?title=0&byline=0&portrait=0" border="0" width="'. $this->params['width'] .'" height="'. $this->params['height'] .'"></iframe>';
            }
        }
        catch (Exception $e) 
        {
            $this->EE->TMPL->log_item('Wyvern Video: Could not load '. $this->params['url']);
        }

        return $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $videos);
    }

    /**
     * Search Vimeo, and return response. 
     * Called only via an ACT request or from $this->vimeo()
     */
    public function load_vimeo($method = FALSE, $params = array(), $return = FALSE)
    {
        @session_start();

        require_once PATH_THIRD .'/wyvern_video/libraries/vimeo/vimeo.php';

        $vimeo = new phpVimeo($this->EE->wyvern_video_helper->vimeo_key, $this->EE->wyvern_video_helper->vimeo_secret);
        $vimeo->enableCache(phpVimeo::CACHE_FILE, $this->cache_path, 300);

        // Set up variables
        $state = isset($_SESSION['vimeo_state']) ? $_SESSION['vimeo_state'] : FALSE;
        $request_token = isset($_SESSION['oauth_request_token']) ? $_SESSION['vimeo_state'] : FALSE;
        $access_token = isset($_SESSION['oauth_access_token']) ? $_SESSION['vimeo_state'] : FALSE;

        // Coming back
        if ($this->EE->input->get('oauth_token') AND $state === 'start') {
            $_SESSION['vimeo_state'] = $state = 'returned';
        }

        // If we have an access token, set it
        if ($access_token) {
            $vimeo->setToken($access_token, $_SESSION['oauth_access_token_secret']);
        }

        $state = isset($_SESSION['vimeo_state']) ? $_SESSION['vimeo_state'] : $state;

        switch ($state) 
        {
            default:
                // Get a new request token
                $token = $vimeo->getRequestToken();

                // Store it in the session
                $_SESSION['oauth_request_token'] = $token['oauth_token'];
                $_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];
                $_SESSION['vimeo_state'] = 'start';

                // Build authorize link
                $authorize_link = $vimeo->getAuthorizeUrl($token['oauth_token'], 'read');

            break;

            case 'returned':

                // Store it
                if ($_SESSION['oauth_access_token'] === NULL && $_SESSION['oauth_access_token_secret'] === NULL) {
                    // Exchange for an access token
                    $vimeo->setToken($_SESSION['oauth_request_token'], $_SESSION['oauth_request_token_secret']);
                    $token = $vimeo->getAccessToken($_REQUEST['oauth_verifier']);

                    // Store
                    $_SESSION['oauth_access_token'] = $token['oauth_token'];
                    $_SESSION['oauth_access_token_secret'] = $token['oauth_token_secret'];
                    $_SESSION['vimeo_state'] = 'done';

                    // Set the token
                    $vimeo->setToken($_SESSION['oauth_access_token'], $_SESSION['oauth_access_token_secret']);
                }

                // Do an authenticated call
                try 
                {
                    $videos = $vimeo->call('vimeo.videos.getUploaded');
                }
                catch (VimeoAPIException $e) 
                {
                    $msg = "Encountered Vimeo API error -- code {$e->getCode()} - {$e->getMessage()}";

                    if ($method)
                    {
                        $this->EE->TMPL->log_item('Wyvern Video: '. $msg);
                    }
                    else
                    {
                        echo $msg;
                    }
                }

            break;
        }

        // Get extra params from $GET
        if ($user_id = $this->EE->input->get('user_id'))
        {
            $params['user_id'] = $user_id;
        }

        $method = $method ? $method : $this->EE->input->get('method');

        switch($method)
        {
            case "videos.search":
                $data = $vimeo->call('vimeo.videos.search', array_merge(array(
                    'query' => $this->EE->input->get('query'),
                    'per_page' => $this->EE->input->get('per_page'),
                    'page' => ($this->EE->input->get('page') ? $this->EE->input->get('page') : 1)
                ), $params));
            break;

            case "videos.getInfo":
                $data = $vimeo->call('vimeo.videos.getInfo', array_merge(array(
                    'video_id' => $this->EE->input->get('video_id')
                ), $params));
            break;

            case "videos.getAll":
                $data = $vimeo->call('vimeo.videos.getAll', array_merge(array(
                    'user_id' => $this->EE->input->get('user_id')
                ), $params));
            break;
        }
        
        if ($return)
        {
            return $data;
        }
        else
        {
            $this->EE->output->enable_profiler(FALSE);
            echo json_encode($data);
            exit;
        }
    }





    /**
     * Cache a response.
     *
     * @param array $params The parameters for the response.
     * @param string $response The serialized response data.
     */
    private function _set_cache($response)
    {
        $hash = md5(serialize($this->params));

        $file = $this->EE->functions->remove_double_slashes($this->cache_path.'/'.$hash.'.cache');

        if (file_exists($file)) 
        {
            unlink($file);
        }

        return file_put_contents($file, $response);
    }

    /**
     * Get the unserialized contents of the cached request.
     *
     * @param array $params The full list of api parameters for the request.
     */
    private function _get_cache()
    {
        $hash = md5(serialize($this->params));

        $file = $this->EE->functions->remove_double_slashes($this->cache_path.'/'.$hash.'.cache');

        if (file_exists($file)) 
        {
            $this->EE->TMPL->log_item('Wyvern Video: Loading '. $this->service .' data from cache '. $file);
            return file_get_contents($file);
        }

        return FALSE;
    }

}
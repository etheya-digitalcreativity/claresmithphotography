<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine Wyvern Video Module Class
 *
 * @package     ExpressionEngine
 * @subpackage  Modules
 * @category    Wyvern Video
 * @author      Brian Litzinger
 * @copyright   Copyright (c) 2010, 2011 - Brian Litzinger
 * @link        http://boldminded.com/add-ons/wyvern-video
 * @license 
 *
 * Copyright (c) 2011, 2012. BoldMinded, LLC
 * All rights reserved.
 *
 * This source is commercial software. Use of this software requires a
 * site license for each domain it is used on. Use of this software or any
 * of its source code without express written permission in the form of
 * a purchased commercial or other license is prohibited.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * As part of the license agreement for this software, all modifications
 * to this source must be submitted to the original author for review and
 * possible inclusion in future releases. No compensation will be provided
 * for patches, although where possible we will attribute each contribution
 * in file revision notes. Submitting such modifications constitutes
 * assignment of copyright to the original author (Brian Litzinger and
 * BoldMinded, LLC) for such modifications. If you do not wish to assign
 * copyright to the original author, your license to  use and modify this
 * source is null and void. Use of this software constitutes your agreement
 * to this clause.
 */

require_once PATH_THIRD.'wyvern_video/config.php';

class Wyvern_video_upd {

    public $version = WYVERN_VIDEO_VERSION;
    
    public function __construct($switch = TRUE)
    {
        $this->EE =& get_instance();

        if (! isset($this->EE->wyvern_video_helper))
        {
            require PATH_THIRD.'wyvern_video/helper.wyvern_video.php';
            $this->EE->wyvern_video_helper = new Wyvern_video_helper;
        }
    }

    public function install()
    {
        // Module data
        $data = array(
            'module_name' => ucwords(WYVERN_VIDEO_SHORT_NAME),
            'module_version' => WYVERN_VIDEO_VERSION,
            'has_cp_backend' => 'y',
            'has_publish_fields' => 'n'
        );

        $this->EE->db->insert('modules', $data);

        $this->EE->load->dbforge();

        if (! $this->EE->db->table_exists('boldminded_settings'))
        {
            $this->EE->dbforge->add_field(array(
                'id'        => array('type' => 'int', 'constraint' => 10, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'site_id'   => array('type' => 'int', 'constraint' => 4, 'unsigned' => TRUE),
                'addon'     => array('type' => 'varchar', 'constraint' => 256),
                'settings'  => array('type' => 'text'),
            ));

            $this->EE->dbforge->add_key('id', TRUE);
            $this->EE->dbforge->create_table('boldminded_settings');

            $this->EE->db->insert('boldminded_settings', array(
                'site_id'   => $this->EE->config->item('site_id'),
                'addon'     => WYVERN_VIDEO_SHORT_NAME,
                'settings'  => json_encode(array())
            ));
        }
        elseif ($this->EE->db->table_exists('boldminded_settings'))
        {
            $exists = $this->EE->db->where('addon', WYVERN_VIDEO_SHORT_NAME)->count_all_results('boldminded_settings');

            if ($exists == 0)
            {
                $this->EE->db->insert('boldminded_settings', array(
                    'addon'     => WYVERN_VIDEO_SHORT_NAME,
                    'settings'  => json_encode($this->EE->wyvern_video_helper->default_settings)
                ));
            }
        }

        // Insert our Action(s)
        $query = $this->EE->db->get_where('actions', array('class' => WYVERN_VIDEO_SHORT_NAME));

        if($query->num_rows() == 0)
        {
            $data = array(
                'class' => WYVERN_VIDEO_SHORT_NAME,
                'method' => 'load_vimeo'
            );

            $this->EE->db->insert('actions', $data);
        }

        // Delete old hooks
        $this->EE->db->query("DELETE FROM exp_extensions WHERE class = '". WYVERN_VIDEO_EXT_NAME ."'");
        
        // Add new hooks
        $ext_template = array(
            'class'    => WYVERN_VIDEO_EXT_NAME,
            'settings' => '',
            'priority' => 5,
            'version'  => WYVERN_VIDEO_VERSION,
            'enabled'  => 'y'
        );
        
        $extensions = array(
            array('hook'=>'sessions_end', 'method'=>'sessions_end'),
            array('hook'=>'publish_form_entry_data', 'method'=>'publish_form_entry_data'),
            array('hook'=>'entry_submission_absolute_end', 'method'=>'entry_submission_absolute_end'),
            array('hook'=>'wygwam_config', 'method'=>'wygwam_config')
        );
        
        foreach($extensions as $extension)
        {
            $ext = array_merge($ext_template, $extension);
            $this->EE->db->insert('exp_extensions', $ext);
        }   

        return TRUE;
    }
    
    public function uninstall()
    {
        $this->EE->db->where('module_name', WYVERN_VIDEO_SHORT_NAME);
        $this->EE->db->delete('modules');

        $this->EE->db->delete('extensions', array('class' => WYVERN_VIDEO_EXT_NAME)); 
        $this->EE->db->delete('actions', array('class' => WYVERN_VIDEO_SHORT_NAME)); 

        $this->EE->db->delete('boldminded_settings', array('addon' => WYVERN_VIDEO_SHORT_NAME));
        
        return TRUE;
    }
    
    public function update($current = '')
    {
        if ( version_compare($current, '0.9.0', '<') )
        {
            $this->update_090();
        }

        if ( version_compare($current, '1.1.0', '<') )
        {
            $this->update_110();
        }

        if ( version_compare($current, '1.2.0', '<') )
        {
            $this->update_120();
        }

        if ( version_compare($current, '1.2.4', '<') )
        {
            $this->update_124();
        }

        return TRUE;
    }

    private function update_124()
    {
        $this->EE->db->where('class', WYVERN_VIDEO_EXT_NAME)
                     ->where('hook', 'sessions_end')
                     ->delete('extensions');
    }

    /*
        Make it MSM compatible and make the settings JSON instead of silly serialized array.
    */
    private function update_120()
    {
        $this->EE->db->query("ALTER TABLE `{$this->EE->db->dbprefix}boldminded_settings` ADD `site_id` int(4) NULL DEFAULT 1 AFTER `id`");

        $settings = json_encode($this->EE->wyvern_video_helper->get_settings());

        $data = array(
            'site_id'   => $this->EE->config->item('site_id'),
            'addon'     => WYVERN_VIDEO_SHORT_NAME,
            'settings' => $settings
        );

        $where = array(
            'site_id'   => $this->EE->config->item('site_id'),
            'addon'     => WYVERN_VIDEO_SHORT_NAME
        );

        $this->EE->wyvern_video_helper->insert_or_update('boldminded_settings', $data, $where, 'id');
    }

    /*
        Stuuupid. The module tags will NOT be recognized unless the first character of the name is uppercased.
    */
    private function update_110()
    {
        $this->EE->db->where('module_name', WYVERN_VIDEO_SHORT_NAME)
                     ->update('modules', array(
                        'module_name' => ucwords(WYVERN_VIDEO_SHORT_NAME)
                     ));
    }

    /*
        Convert all the field settings to JSON instead of serialized arrays.
    */
    private function update_090()
    {
        $qry = $this->EE->db->select('field_id')
                            ->where('field_type', 'wyvern_video')
                            ->get('channel_fields');

        $fields = array();

        foreach ($qry->result() as $row)
        {
            $fields[] = 'field_id_'. $row->field_id;
        }

        if ( ! empty($fields))
        {
            $qry = $this->EE->db->select('entry_id, '. implode(',', $fields) )
                                ->get('channel_data');

            foreach ($qry->result() as $row)
            {
                $data = array();

                foreach ($fields as $field)
                {
                    $value = unserialize(base64_decode($row->$field));
                    $data[$field] = json_encode($value);
                }

                $this->EE->db->where('entry_id', $row->entry_id)->update('channel_data', $data);
            }
        }

        // Don't assume matrix is installed, duh.
        if (array_key_exists('matrix', $this->EE->addons->get_installed()))
        {
            // Now take care of Matrix rows
            $qry = $this->EE->db->select('col_id')
                                ->where('col_type', 'wyvern_video')
                                ->get('matrix_cols');

            $cols = array();

            foreach ($qry->result() as $row)
            {
                $cols[] = 'col_id_'. $row->col_id;
            }

            if ( ! empty($cols))
            {
                $qry = $this->EE->db->select('row_id, '. implode(',', $cols) )
                                    ->get('matrix_data');

                foreach ($qry->result() as $row)
                {
                    $data = array();

                    foreach ($cols as $col)
                    {
                        $value = unserialize(base64_decode($row->$col));
                        $data[$col] = json_encode($value);
                    }

                    $this->EE->db->where('row_id', $row->row_id)->update('matrix_data', $data);
                }
            }
        }
    }
}
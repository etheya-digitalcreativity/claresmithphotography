<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'cartthrob/mod.cartthrob.php';

class Cartthrob_item_options extends Cartthrob
{
	var $return_data = '';
	var $settings = array(); 
	var $module_name = "cartthrob_item_options"; 
	
	public $cartthrob, $store, $cart;
	
	public function __construct()
	{
		$this->EE =& get_instance();
	 	$this->EE->load->library('get_settings');
		$this->settings = $this->EE->get_settings->settings($this->module_name);
		$this->EE->lang->loadfile($this->module_name, $this->module_name);
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		#$this->EE->load->library('cartthrob_loader');
		
 
		$this->EE->load->library('cartthrob_loader');
		$this->EE->cartthrob_loader->setup($this);
		
		$this->EE->lang->loadfile('cartthrob');
		
		$this->EE->load->helper(array('security', 'data_formatting', 'credit_card', 'form'));
		
		$this->EE->load->model('product_model');
		$this->EE->product_model->load_products($this->EE->cartthrob->cart->product_ids());
		
		$this->EE->load->helper('debug');		
	}
 	
 
}


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property CI_Controller $EE
 * @property Cartthrob_cart $cart
 * @property Cartthrob_store $store
 */
class Cartthrob_item_options_mcp
{
	private $module_name = "cartthrob_item_options"; 
	public $required_settings = array();
	public $template_errors = array();
	public $templates_installed = array();
	public $extension_enabled = 0;
	public $module_enabled = 0;
	public $version;
	
	private $initialized = FALSE;
	
	public $nav = array(
 	
	);
	
	public $no_nav = array(

	); 
	private $remove_keys = array(
		'name',
		'submit',
		'x',
		'y',
		'templates',
		'XID',
	);
	
	public $cartthrob, $store, $cart;
	
	public function __construct()
	{
		$this->EE =& get_instance();
		include PATH_THIRD.$this->module_name.'/config/config'.EXT;
 
		$this->EE->load->library('get_settings');
		$this->settings = $this->EE->get_settings->settings($this->module_name);
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/'); 
		$this->EE->load->library('cartthrob_loader'); 
		
	}
	
	private function initialize()
	{
		$this->EE->load->helper(array('security',  'form',  'string', 'data_formatting'));
		$this->EE->load->model(array('field_model', 'channel_model'));
		
		$this->module_enabled = (bool) $this->EE->db->where('module_name', ucwords($this->module_name))->count_all_results('modules');
		$this->extension_enabled = (bool) $this->EE->db->where(array('class' => ucwords($this->module_name).'_ext', 'enabled' => 'y'))->count_all_results('extensions');
		$this->EE->lang->loadfile($this->module_name, $this->module_name);
		
		$this->nav = array(
			'view' => array(
				'view' => $this->EE->lang->line('nav_'.'view'. "_". $this->module_name),
			),
			'add'	=> array(
				'add'	=> $this->EE->lang->line('nav_'.'add'. "_". $this->module_name),
 			),
			'edit'	=> array(
				'edit'	=> $this->EE->lang->line('nav_'.'edit'. "_". $this->module_name),
 			),
			'delete' => array(
				'delete' => $this->EE->lang->line('nav_'.'delete'. "_". $this->module_name),
			),
			'system_settings' => array(
				'system_settings' => $this->EE->lang->line('nav_general_settings'),
			),
		); 
		if ($this->EE->db->count_all($this->module_name.'_options') == 0)
		{
			unset($this->nav['view']); 
		}
		$this->no_form = array(
			'edit',
			'delete',
			'add',
			'view',
  		);
		$this->no_nav = array(
			'edit',
			'delete'
		);
	}
	
	public function index()
	{	
		$this->initialize();
		if (isset($this->nav['view']))
		{
			$method = "view"; 
		}
		else
		{
 			$method = "add"; 
		}
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method='.$method);
	}
 	public function system_settings()
	{
		$this->EE->load->model('channel_model');
		$channels = $this->EE->channel_model->get_channels()->result_array();
 		
		$channel_titles = array("all" => "All", "none" => "None"); // @TODO lang

		$product_channels = $this->EE->cartthrob->store->config('product_channels'); 
		
		foreach ($channels as $channel)
		{
			if (in_array($channel['channel_id'], $product_channels))
			{
				$channel_titles[$channel['channel_id']] = $channel['channel_title'];
			}
		}
 
		
		$structure['class']	= 'global_settings'; 
		$structure['description']	=''; 
		$structure['caption']	=''; 
		$structure['title']	=  $this->module_name.'_system_settings_title';
	 	$structure['settings'] = array(
			array(
				'name' => $this->module_name."_channels",
				'short_name' => "global_item_options_channels",
				'type' => 'multiselect',
				'options' => $channel_titles
			)
		); 
		
		return $this->load_view(__FUNCTION__, array(), $structure);
 
		
	}
	public function view()
	{
		$table = $this->module_name. "_options"; 
		$limit="50";
		$offset = $this->EE->input->get_post('rownum');
		
		$this->EE->load->model('generic_model');
		$model = new Generic_model($table);
		
		$pagination = $this->get_pagination($table,$limit, $offset); 
 		if ($pagination === FALSE)
		{
			$this->EE->session->set_flashdata($this->module_name.'_system_error', sprintf('%s', lang($this->module_name.'_none') ));
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=add');
		}
		$data = $model->read($id=NULL,$order_by=NULL,$order_direction='asc',$field_name=NULL,$string=NULL,$limit,$offset); 

		return $this->load_view(
					__FUNCTION__,
					array(
						__FUNCTION__ => $data, 
						'pagination' => $pagination,
						'form_edit'	=> form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method='.__FUNCTION__.AMP.'return='.__FUNCTION__),
						'delete_href'	=> BASE.AMP. 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=delete'.AMP.'id=',
						'edit_href'	=> BASE.AMP. 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=edit'.AMP.'id=',
					)
				);
				
				
	}
	
	public function add()
	{
		$plugin_vars = array(
			'cartthrob_mcp' => $this,
			'settings' => array(
				"data" => array(
					"data" => "", // @todo, probably need to populate this with stuff in order to edit these. 
				),
			),
			'plugin_type' => 'global',
			'plugins' => array(
				array(
					'classname' => "sub",
					'title' => $this->module_name.'_data_title',
					'overview' => $this->module_name.'_data_overview',
					'settings' => array(
						array(
							'name' => $this->module_name."_data",
							'short_name' => "data",
							'type' => 'matrix',
							'settings' => array(
								array(
									'name' => 'option_value',
									'short_name' => 'option_value',
									'type' => 'text'
								),
								array(
									'name' => 'option_name',
									'short_name' => 'option_name',
									'type' => 'text'
								),
								array(
									'name' => 'price',
									'short_name' => 'price',
									'type' => 'text'
								),
							)
						)
					)
				)
			),
		);
		
		$data["list"] =  $this->EE->load->view($this->module_name.'_plugin_settings', $plugin_vars, TRUE);
 		$data['form_edit'] = form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=form_update'.AMP.'return=view');

		return $this->load_view(__FUNCTION__, $data);
	}
	
	public function edit()
	{
		$table = $this->module_name. "_options"; 
		$this->EE->load->model('generic_model');
		$model = new Generic_model($table);
		$offset = $this->EE->input->get_post('rownum');
		
		$item = $model->read($id=$this->EE->input->get('id') ,$order_by=NULL,$order_direction='asc',$field_name=NULL,$string=NULL,$limit=200,$offset = 0); 
 		
		$item_options = _unserialize($item['data']); 
		
 		$plugin_vars = array(
			'cartthrob_mcp' => $this,
			'settings' => array(
				"sub_settings" => array(
					"data" => $item_options, // @todo, probably need to populate this with stuff in order to edit these. 
				),
			),
			'plugin_type' => 'global',
			'plugins' => array(
				array(
					'classname' => "sub",
					'title' => $this->module_name.'_data_title',
					'overview' => $this->module_name.'_data_overview',
					'settings' => array(
						array(
							'name' => $this->module_name."_data",
							'short_name' => "data",
							'type' => 'matrix',
							'settings' => array(
								array(
									'name' => 'option_value',
									'short_name' => 'option_value',
									'type' => 'text'
								),
								array(
									'name' => 'option_name',
									'short_name' => 'option_name',
									'type' => 'text'
								),
								array(
									'name' => 'price',
									'short_name' => 'price',
									'type' => 'text'
								),
							)
						)
					)
				)
			),
		);

		$data['view']	= $item; 
		$data["list"] =  $this->EE->load->view($this->module_name.'_plugin_settings', $plugin_vars, TRUE);
 		$data['form_edit'] = form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=form_update'.AMP.'return=view');

		return $this->load_view(__FUNCTION__, $data);
	}
	
	public function delete()
	{
		// @TODO if there are no more option groups, this will error out due to too many redirects. 
		$table = $this->module_name. "_options"; 
		$this->EE->load->model('generic_model');
		$model = new Generic_model($table);
		$offset = $this->EE->input->get_post('rownum');
		$data = $model->read($id=$this->EE->input->get('id') ,$order_by=NULL,$order_direction='asc',$field_name=NULL,$string=NULL,$limit=200,$offset = 0); 
 
		$data['member_info'] = array(); 
		if (!empty($data['member_id']))
		{
			$data['member_info'] = $this->get_member_info($data['member_id']); 
		}
 		return $this->load_view(
					__FUNCTION__,
					array(
						'view' => $data, 
						'form_edit'	=> form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=form_update'.AMP.'return=view')
					)
				);
	}
	
	public function form_update()
	{
		$this->initialize();
 		
		$table = $this->module_name. "_options"; 
		$this->EE->load->model('generic_model');
		$model = new Generic_model($table);
		
 		
		if ( $this->EE->input->post('delete'))
		{
			$model->delete($this->EE->input->post('id')); 
			$this->EE->session->set_flashdata($this->module_name.'_system_message', sprintf('%s', lang($this->module_name.'_deleted')));
		}
		else
		{
			foreach (array_keys($_POST) as $key)
			{
				if ( ! in_array($key, $this->remove_keys) && ! preg_match('/^('.ucwords($this->module_name).'_.*?_settings)_.*/', $key))
				{
					$data[$key] = $this->EE->input->post($key, TRUE);
				}
			}
			
			if (isset($data["sub_settings"]["data"]))
			{
				$data['data'] = serialize($data["sub_settings"]["data"]); 
			}
			
			#var_dump($data); exit; 
 			if (!$this->EE->input->post('id'))
			{
 				$model->create($data);
			}
			else
			{
				if ( $this->EE->input->post('id') && !empty($data))
				{
					$model->update($this->EE->input->post('id'), $data);
				}

			}
			$this->EE->session->set_flashdata($this->module_name.'_system_message', sprintf('%s',lang($this->module_name.'_updated')));
		}
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method='.$this->EE->input->get('return', TRUE));
	}
 
	/// GENERIC
	
 	private function pagination_config($method, $total_rows, $per_page=50)
	{
		$config['base_url'] = BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method='.$method;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'rownum';
		$config['full_tag_open'] = '<p id="paginationLinks">';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_prev_button.gif" width="13" height="13" alt="<" />';
		$config['next_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_next_button.gif" width="13" height="13" alt=">" />';
		$config['first_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_first_button.gif" width="13" height="13" alt="< <" />';
		$config['last_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_last_button.gif" width="13" height="13" alt="> >" />';

		return $config;
	}
	public function get_pagination($table, $limit, $offset = NULL)
	{
		if ( ! $offset )
		{		
			$offset = $this->EE->input->get_post('rownum');
		}
		$this->EE->load->library('pagination');
		$total = $this->EE->db->count_all($table);
		if ($total == 0)
		{
			return FALSE; 
		}
		$this->EE->pagination->initialize( $this->pagination_config($this->module_name, $total, $limit) );

		return $this->EE->pagination->create_links();
	}
	public function quick_save()
	{
		$this->initialize();
		$this->EE->load->library('get_settings');
 		
		$db_settings = $this->EE->get_settings->settings($this->module_name,$saved_settings = TRUE);	
			
		$data = array();
		
		foreach (array_keys($_POST) as $key)
		{
			if ( ! in_array($key, $this->remove_keys) && ! preg_match('/^('.ucwords($this->module_name).'_.*?_settings)_.*/', $key))
			{
				$data[$key] = $this->EE->input->post($key, TRUE);
			}
		}
 		foreach ($data as $key => $value)
		{
			$where = array(
				'site_id' => $this->EE->config->item('site_id'),
				'`key`' => $key
			);
			
			if (is_array($value))
			{
				$row['serialized'] = 1;
				$row['value'] = serialize($value);
			}
			else
			{
				$row['serialized'] = 0;
				$row['value'] = $value;
			}
			if (isset($db_settings[$key]))
			{
				if ($value !== $db_settings[$key])
				{
					$this->EE->db->update($this->module_name.'_settings', $row, $where);
				}
			}
			else
			{
 				$this->EE->db->insert($this->module_name.'_settings', array_merge($row, $where));
 			}
		}
		
		$this->EE->session->set_flashdata('message_success', sprintf('%s', lang('settings_saved')));
		
		$return = ($this->EE->input->get('return')) ? AMP.'method='.$this->EE->input->get('return', TRUE) : '';
		
		if ($this->EE->input->post($this->module_name.'_tab'))
		{
			$return .= '#'.$this->EE->input->post($this->module_name.'_tab', TRUE);
		}
		
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.$return);
	}
	private function get_cartthrob_settings()
	{
		$this->EE->load->library('get_settings');
		if (class_exists('cartthrob'))
		{
			return $this->EE->get_settings->settings("cartthrob");
			
		}
		return array(); 
		
	}
	public function get_templates()
	{
		static $templates;
		
		if (is_null($templates))
		{
			$templates = array();
			
			$this->EE->load->model('template_model');
			
			$query = $this->EE->template_model->get_templates();
			
			foreach ($query->result() as $row)
			{
				$templates[$row->group_name.'/'.$row->template_name] = $row->group_name.'/'.$row->template_name;
			}
		}
		
		return $templates;
	}
	private function load_view($current_nav, $more = array(), $structure = array())
	{
 		$this->initialize();
		
		//$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line($this->module_name.'_module_name').' - '.$this->EE->lang->line('nav_head_'.$current_nav));
		
		$vars = array();
		
 		$nav = $this->nav;
		
		$settings_views = array();
		
		$view_paths = array();

		if (is_array($settings_views) && count($settings_views))
		{
			foreach ($settings_views as $key => $value)
			{
				if (is_array($value))
				{
					if (isset($value['path']))
					{
						$view_paths[$key] = $value['path'];
					}
					
					if (isset($value['title']))
					{
						$nav['more_settings'][$key] = $value['title'];
					}
				}
				else
				{
					$nav['more_settings'][$key] = $value;
				}
			}
		}
		
		$sections = array();
 
		foreach ($nav as $top_nav => $subnav)
		{
			if ($top_nav != $current_nav)
			{
				continue;
			}
			
			foreach ($subnav as $url_title => $section)
			{
				if ( ! preg_match('/^http/', $url_title))
				{
					$sections[] = $url_title;
				}
			}
		}
		// we need to get CartThrob's settings for this too. DON'T CHANGE THIS. It's not a mistake. 
 		$settings =  array_merge((array) $this->settings,$this->get_cartthrob_settings()); 
		
		$channels = $this->EE->channel_model->get_channels()->result_array();
 		
		$fields = array();
		
		$channel_titles = array();

		$product_channels = $this->EE->cartthrob->store->config('product_channels'); 
		
		foreach ($channels as $channel)
		{
			if (in_array($channel['channel_id'], $product_channels))
			{
				$channel_titles[$channel['channel_id']] = $channel['channel_title'];

	 			$fields[$channel['channel_id']] = $this->EE->field_model->get_fields($channel['field_group'])->result_array();

				$statuses[$channel['channel_id']] = $this->EE->channel_model->get_channel_statuses($channel['status_group'])->result_array();
			}
		}
		
		
		$data = array(
			'structure'	=> $structure, 
			'nav' => $nav,
			'channels' => $channels,
			'settings_mcp' => $this,
			'cp_page_title' => $this->EE->lang->line($this->module_name.'_module_name').' - '.$this->EE->lang->line('nav_head_'.$current_nav),
			
			'channel_titles' => $channel_titles,
			'fields' => $fields,
			'sections' => $sections,
			'view_paths' => $view_paths,
			'module_name'	=> $this->module_name,
			$this->module_name.'_tab' => (isset($this->settings[$this->module_name.'_tab'])) ? $this->settings[$this->module_name.'_tab'] : 'system_settings',
			$this->module_name.'_mcp' => $this,
			'form_open' => form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->module_name.AMP.'method=quick_save'.AMP.'return='.$this->EE->input->get('method', TRUE)),
			'extension_enabled' => $this->extension_enabled,
			'module_enabled' => $this->module_enabled,
			'settings' => $settings,
			'no_form' => (in_array($current_nav, $this->no_form)),
			'no_nav'	=>  $this->no_nav
		);
		
		if (!empty($structure))
		{
			$data['html'] = $this->EE->load->view($this->module_name.'_settings_template', $data, TRUE);
		}
		
 		$data = array_merge($data, $more);
		
		$self = $data;
		
		$data['data'] = $self;
		
		unset($self);
		
		$this->EE->cp->add_js_script('ui', 'accordion');
		
		$this->EE->cp->add_to_foot($this->EE->load->view($this->module_name.'_settings_form_head', $data, TRUE));
		
		return $this->EE->load->view($this->module_name.'_settings_form', $data, TRUE);
	}
	
 	public function get_member_info($member_id)
	{
		return $this->EE->db->select("*")->where('member_id', $member_id)
 											->limit(1)
											->get("members")
											->row_array();
						
	}

	public function version()
	{
		if (is_null($this->version))
		{
			include PATH_THIRD.$this->module_name.'/config.php';
			$this->version = $config['version'];
		}
		
		return $this->version;
	}
	
	protected function html($content, $tag = 'p', $attributes = '')
	{
		if (is_array($attributes))
		{
			$attributes = _parse_attributes($attributes);
		}
		
		return '<'.$tag.$attributes.'>'.$content.'</'.$tag.'>';
	}
	// --------------------------------
	//  Plugin Settings
	// --------------------------------
	/**
	 * Creates setting controls
	 * 
	 * @access private
	 * @param string $type text|textarea|radio The type of control that is being output
	 * @param string $name input name of the control option
	 * @param string $current_value the current value stored for this input option
	 * @param array|bool $options array of options that will be output (for radio, else ignored) 
	 * @return string the control's HTML 
	 * @since 1.0.0
	 * @author Rob Sanchez
	 */
	public function plugin_setting($type, $name, $current_value, $options = array(), $attributes = array())
	{
		$output = '';
		
		if ( ! is_array($options))
		{
			$options = array();
		}
		else
		{
			foreach ($options as $key => $value)
			{
				$options[$key] = lang($value);
			}
		}
		
		if ( ! is_array($attributes))
		{
			$attributes = array();
		}
 		switch ($type)
		{
			case 'select':
				if (empty($options)) $attributes['value'] = $current_value;
				$output = form_dropdown($name, $options, $current_value, _attributes_to_string($attributes));
				break;
			case 'multiselect':
				$output = form_multiselect($name."[]", $options, $current_value, _attributes_to_string($attributes));
				break;
			case 'checkbox':
				$output = form_label(form_checkbox($name, 1, ! empty($current_value), isset($options['extra']) ? $options['extra'] : '').'&nbsp;'.(!empty($options['label'])? $options['label'] : $this->EE->lang->line('yes') ), $name);
				break;
			case 'text':
				$attributes['name'] = $name;
				$attributes['value'] = $current_value;
				$output =  form_input($attributes);
				break;
			case 'textarea':
				$attributes['name'] = $name;
				$attributes['value'] = $current_value;
				$output =  form_textarea($attributes);
				break;
			case 'radio':
				if (empty($options))
				{
					$output .= form_label(form_radio($name, 1, (bool) $current_value).'&nbsp;'. $this->EE->lang->line('yes'), $name, array('class' => 'radio'));
					$output .= form_label(form_radio($name, 0, (bool) ! $current_value).'&nbsp;'. $this->EE->lang->line('no'), $name, array('class' => 'radio'));
				}
				else
				{
					//if is index array
					if (array_values($options) === $options)
					{
						foreach($options as $option)
						{
							$output .= form_label(form_radio($name, $option, ($current_value === $option)).'&nbsp;'. $option, $name, array('class' => 'radio'));
						}
					}
					//if associative array
					else
					{
						foreach($options as $option => $option_name)
						{
							$output .= form_label(form_radio($name, $option, ($current_value === $option)).'&nbsp;'. lang($option_name), $name, array('class' => 'radio'));
						}
					}
				}
				break;
			default:
		}
		return $output;
	}
	// END
}
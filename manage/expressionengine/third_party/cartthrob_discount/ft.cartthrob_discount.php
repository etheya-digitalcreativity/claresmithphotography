<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once PATH_THIRD.'cartthrob/config.php';

require_once PATH_THIRD.'cartthrob/fieldtypes/ft.cartthrob_settings.php';

class Cartthrob_discount_ft extends Cartthrob_settings_ft
{
	public $info = array(
		'name' => 'CartThrob Discount Settings',
		'version' => CARTTHROB_VERSION,
	);
	
	/**
	 * Display Field on Publish
	 *
	 * @access	public
	 * @param	$data
	 * @return	field html
	 *
	 */
	public function display_field($data)
	{
		if (empty($this->EE->session->cache[__CLASS__]['display_field']))
		{
			$options = array();
			
			$this->fields = array();
			
			$this->EE->load->library('api/api_cartthrob_discount_plugins');
			
			foreach ($this->EE->api_cartthrob_discount_plugins->get_plugins() as $type => $plugin)
			{
				$options[$type] = lang($plugin['title']);
				
				foreach ($plugin['settings'] as $setting)
				{
					$setting['plugin_type'] = $type;
					
					$this->fields[] = $setting;
				}
			}
			
			foreach ($this->EE->api_cartthrob_discount_plugins->global_settings() as $setting)
			{
				$this->fields[] = $setting;
			}
			
			array_unshift($this->fields, array(
				'type' => 'select',
				'name' => 'Type',
				'short_name' => 'type',
				'extra' => ' class="cartthrob_discount_plugin"',
				'options' => $options
			));
		
			$this->EE->load->library('javascript');
			
			$this->EE->javascript->output('
				$(".cartthrob_discount_plugin").bind("change", function() {
					$(this).parents("table").eq(0).find("tbody tr").not(".global").hide().find(":input").attr("disabled", true);
					$(this).parents("table").eq(0).find("tbody tr."+$(this).val()).show().find(":input").attr("disabled", false);
				}).change();
			');
			
			$this->EE->session->cache[__CLASS__]['display_field'] = TRUE;
		}
		
		return parent::display_field($data);
	}
}

/* End of file ft.cartthrob_discount.php */
/* Location: ./system/expressionengine/third_party/cartthrob_discount/ft.cartthrob_discount.php */
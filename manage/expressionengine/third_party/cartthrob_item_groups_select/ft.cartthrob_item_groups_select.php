<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cartthrob_item_groups_select_ft extends EE_Fieldtype {

	var $module_name = "cartthrob_item_options"; 
	var $info = array(
		'name'		=> 'CartThrob Global Item Groups Multi Select',
		'version'	=> '1.0'
	);
	var $has_array_data = TRUE;

	public function __construct()
	{
		parent::__construct();
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');
		
		$this->EE->load->library('cartthrob_loader');
		$this->EE->load->library('number');
		$this->EE->load->helper('data_formatting');
	}

	function display_field($data)
	{
		$this->EE->load->helper('custom_field');

		$field_options = array_merge(array("" => '---'),$this->_get_field_options($data) );
		
		$selected = decode_multi_field($data);

 		return form_multiselect($this->field_name.'[]', $field_options, $selected, 'dir="ltr" id="'.$this->field_id.'" style="width:250px"');
		
	}
	function save($data)
	{
 		$this->EE->load->helper('custom_field');
		
		return encode_multi_field($data); 
	}

 
	// --------------------------------------------------------------------
 	function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		$this->EE->load->helper('custom_field');
		$data = decode_multi_field($data);

		if ($tagdata)
		{
			return $this->_parse_multi($data, $params, $tagdata);
		}
		else
		{
			return $this->_parse_single($data, $params);
		}
	}
	
	// --------------------------------------------------------------------
	
	function _parse_single($data, $params)
	{
		if (isset($params['limit']))
		{
			$limit = intval($params['limit']);

			if (count($data) > $limit)
			{
				$data = array_slice($data, 0, $limit);
			}
		}

		if (isset($params['markup']) && ($params['markup'] == 'ol' OR $params['markup'] == 'ul'))
		{
			$entry = '<'.$params['markup'].'>';

			foreach($data as $dv)
			{
				$entry .= '<li>';
				$entry .= $dv;
				$entry .= '</li>';
			}

			$entry .= '</'.$params['markup'].'>';
		}
		else
		{
			$entry = implode('|', $data);
		}

		return $this->EE->typography->parse_type(
				$this->EE->functions->encode_ee_tags($entry),
				array(
						'text_format'	=> "none", //$this->row['field_ft_'.$this->field_id],
						'html_format'	=> $this->row['channel_html_formatting'],
						'auto_links'	=> $this->row['channel_auto_link_urls'],
						'allow_img_url' => $this->row['channel_allow_img_urls']
					  )
		);
	}
	
	// --------------------------------------------------------------------
	
	function _parse_multi($data, $params, $tagdata)
	{
		$chunk = '';
		$limit = FALSE;

		// Limit Parameter
		if (is_array($params) AND isset($params['limit']))
		{
			$limit = $params['limit'];
		}

		foreach($data as $key => $item)
		{
			if ( ! $limit OR $key < $limit)
			{
				$vars['item'] = $item;
				$vars['count'] = $key + 1;	// {count} parameter

				$tmp = $this->EE->functions->prep_conditionals($tagdata, $vars);
				$chunk .= $this->EE->functions->var_swap($tmp, $vars);
			}
			else
			{
				break;
			}
		}

		// Everybody loves backspace
		if (isset($params['backspace']))
		{
			$chunk = substr($chunk, 0, - $params['backspace']);
		}
		
		// Typography!
		return $this->EE->typography->parse_type(
						$this->EE->functions->encode_ee_tags($chunk),
						array(
								'text_format'	=> "none", //$this->row['field_ft_'.$this->field_id],
								'html_format'	=> $this->row['channel_html_formatting'],
								'auto_links'	=> $this->row['channel_auto_link_urls'],
								'allow_img_url' => $this->row['channel_allow_img_urls']
							  )
		);
	}
 	
	function _get_field_options($data)
	{
		$field_options = array();
 
		$table = $this->module_name. "_options"; 
		$this->EE->load->model('generic_model');
		$model = new Generic_model($table);
 		
		$options = (array) $model->read(NULL ,$order_by=NULL,$order_direction='asc',$field_name=NULL,$string=NULL,$limit=200,$offset = 0); 
 		
		#$item_options = _unserialize($item['data']);
		
		if ($options)
		{
			foreach ($options as $option)
			{
				$field_options[$option['short_name']] = $option['label']; 
			}
		}
		
 		return $field_options;
	}
 }

// END Member_group_multi_select_ft class